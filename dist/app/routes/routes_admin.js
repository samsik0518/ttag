'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _artist = require('./../models/artist');

var _artist2 = _interopRequireDefault(_artist);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// app/routes.js
var router = _express2.default.Router();

router.get('/', function (req, res) {

    res.render('admin/index', { title: 'T-Tag', isLoggedIn: false }); // load the index.ejs file
}).get('/artist_add', function (req, res) {
    var body = '';

    _artist2.default.schema.eachPath(function (path) {
        //console.log(path);
        body += '<label for="' + path + '">' + path + '</label>' + '<input type="text"  class="form-control" id="' + path + '">';
    });

    res.render('admin/artist_add', { title: 'T-Tag 수동 아티스트 등록', body_form: body });
});

exports.default = router;
//# sourceMappingURL=routes_admin.js.map