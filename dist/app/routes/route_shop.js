'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _term = require('../models/term');

var _term2 = _interopRequireDefault(_term);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _uploadS = require('../controllers/uploadS3');

var _uploadS2 = _interopRequireDefault(_uploadS);

var _basic = require('../controllers/basic');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Controllers


//Models
// app/routes.js
//Modules
var router = _express2.default.Router();

router.get('/', function (req, res) {
    if (req.isAuthenticated()) {
        if (req.user.isNewUser) res.redirect('/setUser'); // load the index.ejs file
        else res.render('index.ejs', { title: 'T-Tag', isLoggedIn: true }); // load the index.ejs file
    } else res.render('index.ejs', { title: 'T-Tag', isLoggedIn: false }); // load the index.ejs file
});

exports.default = router;
/*

export default function(app, passport) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res) {
        if (isLoggedIn()) {
            if(req.user.isNewUser)
                res.redirect('/setUser'); // load the index.ejs file
            else
                res.render('index.ejs',{ title: 'T-Tag', isLoggedIn: isLoggedIn() }); // load the index.ejs file

        }
        else
            res.render('index.ejs',{ title: 'T-Tag', isLoggedIn: isLoggedIn() }); // load the index.ejs file

    });
    // =====================================
    // LOGOUT ROUTES =======================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    // =====================================
    // SET INFO ROUTES =====================
    // =====================================
    app.get('/setUser', isLoggedIn, function(req, res) {

        var social_account = [];
        var checked = [];
        var name="", nickname="", phone="", postalCode="", basicAddr="", detailAddr="", email="", birthday="";
        var p_name, p_nickname, p_phone, p_postalCode, p_basicAddr, p_detailAddr, p_email="", p_birthday="";
        var advDm = false, advEmail=false;
        if(req.user) {
            if (!isEmpty(req.user.naver.provider)) {
                social_account.push("naver");
            }
            if (!isEmpty(req.user.google.provider)) {
                social_account.push("google");
            }
            if (!isEmpty(req.user.kakaotalk.provider)) {
                social_account.push("kakaotalk");
            }
            if (!isEmpty(req.user.facebook.provider)) {
                social_account.push("facebook");
            }
            if (!isEmpty(req.user.instagram.provider)) {
                social_account.push("instagram");

            }
            console.log("social Account : " + social_account);
            User.findById(req.user._id, function(err, user){
                if(!isEmpty(user.userInfo.terms)) {
                    checked.push({"terms" : user.userInfo.terms});
                }
                if(!isEmpty(user.userInfo.privateInfo)) {
                    checked.push({"privacy" : user.userInfo.privateInfo});
                }
                if(!isEmpty(user.userInfo.advertise)) {
                    checked.push({"advertise" : user.userInfo.advertise});
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                if(!isEmpty(user.userInfo.name)){
                    name = user.userInfo.name;
                }else{
                    p_name = "이름을 입력해주세요.";
                }
                if(!isEmpty(user.userInfo.nickname)){
                    nickname = user.userInfo.nickname;
                }else{
                    p_nickname="별명을 입력해주세요."
                }
                if(!isEmpty(user.userInfo.phone)){
                    console.log("phone : " +user.userInfo.phone)
                    phone= user.userInfo.phone;
                }else{
                    p_phone="'-' 없이 입력해주세요."
                }
                if(!isEmpty(user.userInfo.postalCode)){
                    postalCode = user.userInfo.postalCode;
                }else{
                    p_postalCode="000-000"
                }
                if(!isEmpty(user.userInfo.basicAddr)){
                    basicAddr = user.userInfo.basicAddr;
                }else{
                    p_basicAddr="주소 검색하기 버튼을 눌러주세요."
                }
                if(!isEmpty(user.userInfo.detailAddr)){
                    detailAddr = user.userInfo.detailAddr;
                }else{
                    p_detailAddr="상세 주소를 입력해주세요."
                }
                if(!isEmpty(user.userInfo.email)){
                    email = user.userInfo.email;
                }else{
                    p_email ="사용하는 이메일 주소를 입력해주세요."
                }
                if(!isEmpty(user.userInfo.birthday)){
                    birthday = user.userInfo.birthday;
                }else{
                    p_birthday ="1900 / 01 / 01 여덟자리 숫자로 입력해주세요"
                }
                if(!isEmpty(user.userInfo.advDm)){
                    advDm = user.userInfo.advDm;
                }else{
                    advDm = false;
                }
                if(!isEmpty(user.userInfo.advEmail)){
                    advEmail = user.userInfo.advEmail;
                }else{
                    advEmail = false;
                }
                var fashion_code = "", p_fashion_code=""
               user.getProfileImage(function (img){
                   var profile_image = img;
                   console.log(p_nickname, p_postalCode, p_basicAddr, p_detailAddr);
                   Terms.findOne().sort({ field: 'asc', _id: -1 }).limit(1).exec(function (err, term) {
                       var terms ={}

                       if(!term){
                           terms.terms = "이용약관을 등록하세요";
                           terms.privacy_required = "개인정보수집이용동의(필수)를 등록하세요";
                           terms.privacy_choosable = "개인정보수집이용동의(선택)를 등록하세요";
                           terms.advertise = "광고정보수신동의를 등록하세요";

                       }else{
                           if(isEmpty(term.terms)){
                               terms.terms = "이용약관을 등록하세요";
                           }else{
                               terms.terms = term.terms;
                           }
                           if(isEmpty(term.privacy_required)){
                               terms.privacy_required = "개인정보수집이용동의(필수)를 등록하세요";
                           }else{
                               terms.privacy_required = term.privacy_required;
                           }
                           if(isEmpty(term.privacy_choosable)){
                               terms.privacy_choosable = "개인정보수집이용동의(선택)를 등록하세요";
                           }else{
                               terms.privacy_choosable = term.privacy_choosable;
                           }
                           if(isEmpty(term.advertise)){
                               terms.advertise = "광고정보수신동의를 등록하세요";
                           }else{
                               terms.advertise = term.advertise;
                           }
                       }

                       var sign_info = '<button id="logout" onclick="location.href=\'/logout\'">Log Out</button>';
                       res.render('setUserInfo.ejs', {
                           title: 'Jisco',
                           sign_info: sign_info,
                           terms:              terms.terms,
                           privacy_required:   terms.privacy_required,
                           privacy_choosable:  terms.privacy_choosable,
                           advertise :         terms.advertise,
                           social_account: social_account,
                           checked: checked,
                           name: name,
                           nickname: nickname,
                           phone: phone,
                           postalCode: postalCode,
                           basicAddr : basicAddr,
                           detailAddr: detailAddr,
                           email: email,
                           birthday: birthday,
                           profile_image: profile_image,
                           fashion_code: fashion_code,
                           advDm: advDm,
                           advEmail : advEmail,
                           p_name: p_name,
                           p_nickname: p_nickname,
                           p_phone: p_phone,
                           p_postalCode: p_postalCode,
                           p_basicAddr : p_basicAddr,
                           p_detailAddr: p_detailAddr,
                           p_email: p_email,
                           p_birthday: p_birthday,
                           p_fashion_code: p_fashion_code

                       });
                   });
               });

            })


        }
        else{
            res.redirect('/');
        }
    });
    app.post('/setUser', function(req, res){
       console.log(req.body);
       User.findById(req.user._id, function(err, user){
           if(err) throw err
           user.setUserInfo(req.body, function (err, userResult){
               if(err) throw err

               userResult.save(function (err){
                   if(err) throw err
                   res.redirect('/setUser');
               })
           })
       })
    });
    app.post('/setUser/profileImage', isLoggedIn, uploadProfile, function(req, res){
            console.log(req.file);
        User.findById(req.user._id, function (err, user){
            if(err) throw err;
            user.userInfo.image = req.file.location;
            user.save(function(err){
                if(err) throw err;
                res.json({'profile_src': user.userInfo.image});
            })
        })

    });

    app.post('/setUser/getProfileImage', isLoggedIn, function(req, res){
        User.findById(req.user._id, function (err, user){
            if(err) throw err;
                res.json({'profile_src': user.userInfo.image});
            })

    });


// =====================================
// SET INFO STOPED =====================
// =====================================

    app.get('/stopSetUserInfo', function(req, res) {
        if(req.user){
            if(req.user.isNewUser){
                var user = req.user;
                user.isNewUser = false;
                user.save(function(err){
                    if (err) throw err
                    else{
                        res.redirect('/');
                    }
                })
            }
        }
        else {
            res.redirect('/');
        }
    });

    // =====================================
    // FACEBOOK ROUTES =====================
    // =====================================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : ['email']}));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',passport.authenticate('facebook', { failureRedirect: '/login' }),  function(req, res) {

        if(!isEmpty(req.user.kakaotalk.provider) || !isEmpty(req.user.instagram.provider) || !isEmpty(req.user.google.provider) || !isEmpty(req.user.naver.provider) ){
            console.log("connect new Account!");
            res.redirect('/setUser');
        }else{
            res.redirect('/');
        }
    });
    // =====================================
    // GOOGLE ROUTES =======================
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',passport.authenticate('google', { failureRedirect: '/login' }),  function(req, res) {

        if(!isEmpty(req.user.kakaotalk.provider) || !isEmpty(req.user.instagram.provider) || !isEmpty(req.user.facebook.provider) || !isEmpty(req.user.naver.provider) ){
            console.log("connect new Account!");

            res.redirect('/setUser');
        }else{
            res.redirect('/');
        }
    });


    // =====================================
    // NAVER ROUTES ========================
    // =====================================
    // route for twitter authentication and login
    app.get('/auth/naver', passport.authenticate('naver'));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/naver/callback',passport.authenticate('naver', { failureRedirect: '/login' }),  function(req, res) {

        if(!isEmpty(req.user.kakaotalk.provider) || !isEmpty(req.user.instagram.provider) || !isEmpty(req.user.facebook.provider) || !isEmpty(req.user.google.provider) ){
            console.log("connect new Account!");
            res.redirect('/setUser');
        }else{
            res.redirect('/');
        }
    });


    // =====================================
    // INSTRAGRAM ROUTES ===================
    // =====================================
    app.get('/auth/instagram', passport.authenticate('instagram'));

    // handle the callback after instagram has authenticated the user
    app.get('/auth/instagram/callback',passport.authenticate('instagram', { failureRedirect: '/login' }),
        function(req, res) {
        if(!isEmpty(req.user.naver.provider) || !isEmpty(req.user.kakaotalk.provider) || !isEmpty(req.user.facebook.provider) || !isEmpty(req.user.google.provider) ){
            console.log("connect new Account!");

            res.redirect('/setUser');
        }else{
            res.redirect('/');
        }
    });

    // =====================================
    // KAKAOTALK ROUTES ====================
    // =====================================
    app.get('/auth/kakaotalk', passport.authenticate('kakao'));

    // handle the callback after kakaotalk has authenticated the user
    app.get('/auth/kakaotalk/callback',passport.authenticate('kakao', { failureRedirect: '/login' }),  function(req, res) {

        if(!isEmpty(req.user.naver.provider) || !isEmpty(req.user.instagram.provider) || !isEmpty(req.user.facebook.provider) || !isEmpty(req.user.google.provider) ){
            console.log("connect new Account!");

            res.redirect('/setUser');
        }else{
            res.redirect('/');
        }
    });
    // =================================================================================================================
    // USER ACCOUNT ROUTES END =========================================================================================
    // =================================================================================================================

    // =================================================================================================================
    // CUSTOMIZING ROUTES START ========================================================================================
    // =================================================================================================================
    app.get('/customizing/onepiece', function(req, res) {
        var neckline = new getOnepiece();
        neckline.neckline(function(res_neckline){
            neckline.bodyShape(function(res_bodyShape){
                neckline.sleevesStyle(function(res_sleevesStyle){
                    if (req.user) {
                        var sign_info = '<button id="logout" onclick="location.href=\'/logout\'">Log Out</button>'
                        res.render('one_piece.ejs',{ title: 'Jisco | Customizing Onepiece', sign_info: sign_info , neckline: res_neckline, bodyShape: res_bodyShape, sleeves: res_sleevesStyle});

                    } else {
                        res.render('one_piece.ejs',{ title: 'Jisco | Customizing Onepiece', sign_info: '<button id="sign" href="">Sign In</button>', neckline: res_neckline, bodyShape: res_bodyShape, sleeves: res_sleevesStyle });
                    }
                })
            })
        });

    });

};



//var storage =   multer.diskStorage({
//    destination: function (req, file, callback) {
//        callback(null, __dirname+'/public/uploads/');
//    },
//    filename: function (req, file, callback) {
//        callback(null, file.fieldname + '-' + Date.now());
//    }
//});
*/

// route middleware to make sure a user is logged in

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}
//# sourceMappingURL=routes_shop.js.map