'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _serveFavicon = require('serve-favicon');

var _serveFavicon2 = _interopRequireDefault(_serveFavicon);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _passport3 = require('./config/passport');

var _passport4 = _interopRequireDefault(_passport3);

var _connectFlash = require('connect-flash');

var _connectFlash2 = _interopRequireDefault(_connectFlash);

var _database = require('./config/database.js');

var _database2 = _interopRequireDefault(_database);

var _expressSession = require('express-session');

var _expressSession2 = _interopRequireDefault(_expressSession);

var _routes_shop = require('./routes/routes_shop');

var _routes_shop2 = _interopRequireDefault(_routes_shop);

var _routes_admin = require('./routes/routes_admin');

var _routes_admin2 = _interopRequireDefault(_routes_admin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var port = process.env.PORT || '8888'; // pass passport for configuration


var app = (0, _express2.default)();

// configuration ===============================================================
_mongoose2.default.Promise = global.Promise;
console.log(_database2.default.url);
_mongoose2.default.connect(_database2.default.url); // connect to our database
(0, _passport4.default)(_passport2.default);

app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});
// view engine setup
app.set('views', _path2.default.join(__dirname, '../views'));
app.set('view engine', 'ejs');
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(_express2.default.static(_path2.default.join(__dirname, '/../public')));
app.use((0, _morgan2.default)('dev'));
app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));

app.use((0, _cookieParser2.default)());
console.log(_path2.default.join(__dirname, '/../public'));
// required for passport
app.use((0, _expressSession2.default)({ secret: 'studiojoosworkthebestcompany' })); // session secret
app.use(_passport2.default.initialize());
app.use(_passport2.default.session());
app.use((0, _connectFlash2.default)()); // use connect-flash for flash messages stored in session


// routes ======================================================================
app.use('/', _routes_shop2.default);
app.use('/admin', _routes_admin2.default);

app.set('port', port);
app.listen(port, function () {
    console.log('Express listening on port', port);
});
//# sourceMappingURL=app.js.map