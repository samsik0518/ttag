'use strict';

// app/models/ttagUser.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the schema for our ttagUser model
var ttagArtistSchema = mongoose.Schema({
    isNewttagUser: Boolean,
    signupDate: Date,
    role: { type: String, default: 'regular' },
    position: { type: String, default: 'customer' },

    facebook: {
        id: String,
        token: String,
        email: String,
        name: String,
        provider: String,
        photo: String,
        json: Object
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        ttagUsername: String,
        provider: String

    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String,
        provider: String,
        photo: String,
        json: Object

    },
    naver: {
        id: String,
        displayName: String,
        name: String,
        email: String,
        provider: String,
        photo: String,
        json: Object
    },
    kakaotalk: {
        id: String,
        name: String,
        ttagUsername: String,
        provider: String,
        roles: String,
        photo: String,
        json: Object
    },
    instagram: {
        id: String,
        name: String,
        displayName: String,
        json: Object,
        provider: String,
        photo: String

    },
    ttagUserInfo: {
        level: { type: String, defalut: '브론즈' },
        terms: Boolean,
        privateInfo: Boolean,
        advertise: Boolean,
        name: String,
        nickname: String,
        postalCode: String,
        basicAddr: String,
        detailAddr: String,
        phone: String,
        email: String,
        image: String,
        advDm: Boolean,
        advEmail: Boolean,
        birthday: Number,
        fashionCode: {
            codeName: String,
            updateDate: Date,
            size: String,
            gender: String,
            height: Number,
            weight: Number,
            color: String,
            style: [String],
            Measure: Boolean
        },
        size: {
            top: {
                shoulder: Number,
                chest: Number,
                stomach: Number,
                belly: Number,
                head: Number,
                neck: Number,
                forearm: Number,
                wrist: Number,
                elbow: Number,
                hand: Number
            },
            bottom: {
                belly: Number,
                hip: Number,
                thigh: Number,
                knee: Number,
                calf: Number,
                ankle: Number,
                heel: Number,
                centerBack: Number,
                crotch: Number,
                kneePosition: Number
            }
        }
    }

});

// methods ======================
// generating a hash
ttagArtistSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
ttagArtistSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

ttagArtistSchema.methods.setttagUserInfo = function (req, callback) {
    console.log("setttagUser Start");
    if (!isEmpty(req.terms)) {

        if (req.terms == 'true' || req.terms) this.ttagUserInfo.terms = true;else this.ttagUserInfo.terms = false;
    }
    if (!isEmpty(req.privacy)) {

        if (req.privacy == 'true' || req.privacy == true) this.ttagUserInfo.privateInfo = true;else this.ttagUserInfo.privateInfo = false;
    }
    if (!isEmpty(req.advertise)) {

        if (req.advertise == 'true' || req.advertise == true) this.ttagUserInfo.advertise = true;else this.ttagUserInfo.advertise = false;
    }

    if (!isEmpty(req.name)) {
        console.log("Terms Exist: " + req.name);
        this.ttagUserInfo.name = req.name;
    }
    if (!isEmpty(req.nickname)) {
        this.ttagUserInfo.nickname = req.nickname;
    }

    if (!isEmpty(req.postalCode)) {
        this.ttagUserInfo.postalCode = req.postalCode;
    }

    if (!isEmpty(req.basicAddr)) {
        this.ttagUserInfo.basicAddr = req.basicAddr;
    }

    if (!isEmpty(req.detailAddr)) {
        this.ttagUserInfo.detailAddr = req.detailAddr;
    }

    if (!isEmpty(req.phone)) {
        this.ttagUserInfo.phone = req.phone;
    }
    callback(null, this);
};
ttagArtistSchema.methods.getProfileImage = function (callback) {
    if (!isEmpty(this.ttagUserInfo.image)) {
        return callback(this.ttagUserInfo.image);
    }
    if (!isEmpty(this.naver)) {
        if (this.naver.json.profile_image != "https://ssl.pstatic.net/static/pwe/address/nodata_33x33.gif") {
            return callback(this.naver.json.profile_image);
        }
    }

    if (!isEmpty(this.facebook)) {
        if (!this.facebook.json.picture.data.is_silhouette) {
            return callback(this.facebook.json.picture.data.url);
        }
    }
    if (!isEmpty(this.google)) {
        if (!this.google.json.image.is_default) {
            return callback(this.google.json.image.url);
        }
    }
    if (!isEmpty(this.google)) {
        if (!this.google.json.image.is_default) {
            return callback(this.google.json.image.url);
        }
    }
    if (!isEmpty(this.kakaotalk)) {
        return callback(this.kakaotalk.json.properties.profile_image);
    }
    if (!isEmpty(this.instagram)) {
        return callback(this.instagram.json.data.profile_picture);
    } else {
        return callback("https://ssl.pstatic.net/static/pwe/address/nodata_33x33.gif");
    }
};

function isEmpty(test) {
    if (test == 'undefined') return true;
    if (typeof test == 'undefined') return true;
    if (test === 'undefined') return true;
    if (typeof test === 'undefined') return true;

    return false;
}
// create the model for ttagUsers and expose it to our app
module.exports = mongoose.model('ttagArtist', ttagArtistSchema);
//# sourceMappingURL=artist.js.map