'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _multerS = require('multer-s3');

var _multerS2 = _interopRequireDefault(_multerS);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _aws = require('./../config/aws');

var _aws2 = _interopRequireDefault(_aws);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var uploadProfile = (0, _multer2.default)({
    storage: (0, _multerS2.default)({
        s3: _aws2.default,
        bucket: 'jisco',
        metadata: function metadata(req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function key(req, file, cb) {
            _crypto2.default.pseudoRandomBytes(16, function (err, raw) {
                cb(null, 'user/profile/' + raw.toString('hex') + _path2.default.extname(file.originalname));
            });
        }
    })
}).single('profileImage');

exports.default = uploadProfile;
//# sourceMappingURL=uploadS3.js.map