"use strict";

module.exports = {
    context: __dirname,
    entry: "./app/app.js",
    output: {
        path: './build',
        filename: "bundle.js"
    },
    resolve: {
        root: __dirname + '/app',
        extensions: ['', '.js']
    },

    module: {
        loaders: [{ test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }, { test: /\.sass$/, loader: 'style!css!sass' }, {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /(node_modules|bower_components)/,
            query: {
                presets: ['es2015']
            }
        }, { test: /\.ejs$/, loader: 'ejs-loader?variable=data' }]
    },
    devtool: 'source-map',
    devServer: {
        port: 9000,
        contentBase: __dirname,
        inline: true
    },
    node: {
        fs: "empty"
    }

};
//# sourceMappingURL=webpack.config.js.map