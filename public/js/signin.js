//////////////////////////////// login via social account ////////////////////////////////
var closeSignInModal = function(){
        document.getElementById('signModal').setAttribute('style', 'display: none');
}
var googleUser = {};
var startApp = function() {
    gapi.load('auth2', function(){
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id: '196572310111-d787oc6rt0nfloufprklujb422ve0e7m.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            scope: 'profile email'
        });
        attachSignin(document.getElementById('custom-google-btn'));
    });
};

function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
            console.log(googleUser.getBasicProfile().getName());
            closeSignInModal();
        }, function(error) {
            alert(JSON.stringify(error, undefined, 2));
            return false
        });
}