/**
 * Created by samsi on 2017-01-04.
 */
// config/connectAccount.js

// load all the things we need
var FacebookStrategy    = require('passport-facebook').Strategy;
var TwitterStrategy     = require('passport-twitter').Strategy;
var GoogleStrategy      = require('passport-google-oauth').OAuth2Strategy;
var NaverStrategy       = require('passport-naver').Strategy;
var InstagramStrategy   = require('passport-instagram').Strategy;
var KakaoStrategy       = require('passport-kakao').Strategy;

// load up the user model
var User            = require('../app/models/user');
// load the auth variables
var configAuth      = require('./auth');
// load timeStamp
var Timestamp            = require('../app/controllers/getDate');
module.exports = function(connectAccount, add) {

    // used to serialize the user for the session
    connectAccount.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    connectAccount.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // code for login (use('local-login', new LocalStategy))
    // code for signup (use('local-signup', new LocalStategy))

    // =========================================================================
    // NAVER ===================================================================
    // =========================================================================

    connectAccount.use(new NaverStrategy({
            clientID: configAuth.naverAuth.clientID,
            clientSecret: configAuth.naverAuth.clientSecret,
            callbackURL: configAuth.naverAuth.callbackURL
        },
        function(accessToken, refreshToken, profile, done) {
            User.findOne({
                'naver.id': profile.id
            }, function(err, user) {
                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);

                // if the user is found then log them in
                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    // if there is no user, create them
                    var newUser                 = new User();

                    // set all of the user data that we need
                    newUser.naver.id            = profile.id;
                    newUser.naver.name          = profile.displayName;
                    newUser.naver.email         = profile.emails[0].value;
                    newUser.naver.provider      = 'naver';
                    newUser.isNewUser           = true;

                    var date = new Timestamp();
                    newUser.signupDate          = date.yyyymmdd();
                    // save our user into the database
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        }
    ));
    // TWITTER =================================================================
    // =========================================================================
    connectAccount.use(new TwitterStrategy({

            consumerKey     : configAuth.twitterAuth.consumerKey,
            consumerSecret  : configAuth.twitterAuth.consumerSecret,
            callbackURL     : configAuth.twitterAuth.callbackURL

        },
        function(token, tokenSecret, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Twitter
            process.nextTick(function() {

                User.findOne({ 'twitter.id' : profile.id }, function(err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found then log them in
                    if (user) {
                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser                 = new User();

                        // set all of the user data that we need
                        newUser.twitter.id          = profile.id;
                        newUser.twitter.token       = token;
                        newUser.twitter.username    = profile.username;
                        newUser.twitter.displayName = profile.displayName;
                        newUser.isNewUser           = true;
                        newUser.twitter.provider    = 'twitter';
                        var date = new Timestamp();
                        newUser.signupDate          = date.yyyymmdd();
                        // save our user into the database
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });

            });

        }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    connectAccount.use(new GoogleStrategy({

            clientID        : configAuth.googleAuth.clientID,
            clientSecret    : configAuth.googleAuth.clientSecret,
            callbackURL     : configAuth.googleAuth.callbackURL,

        },
        function(token, refreshToken, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Google
            process.nextTick(function() {

                // try to find the user based on their google id
                User.findOne({ 'google.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        // if a user is found, log them in
                        return done(null, user);
                    } else {
                        // if the user isnt in our database, create a new user
                        var newUser          = new User();

                        // set all of the relevant information
                        newUser.google.id    = profile.id;
                        newUser.google.token = token;
                        newUser.google.name  = profile.displayName;
                        newUser.google.email = profile.emails[0].value; // pull the first email
                        newUser.google.provider = 'google'
                        newUser.isNewUser           = true;
                        var date = new Timestamp();
                        newUser.signupDate          = date.yyyymmdd();
                        // save the user
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });
            });

        }));


    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    connectAccount.use(new FacebookStrategy({

            // pull in our app id and secret from our auth.js file
            clientID        : configAuth.facebookAuth.clientID,
            clientSecret    : configAuth.facebookAuth.clientSecret,
            callbackURL     : configAuth.facebookAuth.callbackURL,
            profileFields   : ["email", "displayName", 'id', 'photos']
        },

        // facebook will send back the token and profile
        function(token, refreshToken, profile, done) {

            // asynchronous
            process.nextTick(function() {

                // find the user in the database based on their facebook id
                User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (user) {
                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user found with that facebook id, create them
                        var newUser            = new User();

                        // set all of the facebook information in our user model
                        newUser.facebook.id    = profile.id; // set the users facebook id
                        newUser.facebook.token = token; // we will save the token that facebook provides to the user
                        newUser.facebook.name  = profile.displayName; // look at the connectAccount user profile to see how names are returned
                        newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                        newUser.facebook.photos = profile.photos[0].value;
                        newUser.facebook.provider = 'facebook';
                        newUser.isNewUser           = true;
                        var date = new Timestamp();
                        newUser.signupDate          = date.yyyymmdd();
                        console.log(newUser.facebook);
                        // save our user to the database
                        newUser.save(function(err) {
                            if (err)
                                throw err;

                            // if successful, return the new user
                            return done(null, newUser);
                        });
                    }

                });
            });

        }));

    // =========================================================================
    // INSTAGRAM ===============================================================
    // =========================================================================
    connectAccount.use(new InstagramStrategy({
            clientID: configAuth.instagramAuth.clientID,
            clientSecret: configAuth.instagramAuth.clientSecret,
            callbackURL: configAuth.instagramAuth.callbackURL,
            passReqToCallback: true,
            passResToCallback: true
        },
        // instagram will send back the token and profile
        function( token, refreshToken, profile, done) {
            console.log("connectAccount : " + add);
            // asynchronous
            if(add == true || add == 'true'){

            }
            else{
                process.nextTick(function () {

                    // find the user in the database based on their facebook id
                    User.findOne({'instagram.id': profile.id}, function (err, user) {

                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database
                        if (err)
                            return done(err);

                        // if the user is found, then log them in
                        if (user) {
                            return done(null, user); // user found, return that user
                        } else {
                            console.log(profile);
                            // if there is no user found with that facebook id, create them
                            var newUser            = new User();

                            // set all of the facebook information in our user model
                            newUser.instagram.id    = profile.id; // set the users facebook id
                            newUser.instagram.name = profile.username; // we will save the token that facebook provides to the user
                            newUser.instagram.displayName  = profile.displayName; // look at the connectAccount user profile to see how names are returned
                            newUser.instagram.json          = profile._json;
                            newUser.instagram.provider      = 'instagram';
                            newUser.isNewUser                 = true;
                            var date = new Timestamp();
                            newUser.signupDate          = date.yyyymmdd();

                            // save our user to the database
                            newUser.save(function(err) {
                                if (err)
                                    throw err;

                                // if successful, return the new user
                                return done(null, newUser);
                            });
                        }

                    });
                });
            }

        }
    ));

    // =========================================================================
    // KAKAOTALK ===============================================================
    // =========================================================================
    connectAccount.use(new KakaoStrategy({
            clientID: configAuth.kakaotalkAuth.clientID,
            callbackURL: configAuth.kakaotalkAuth.callbackURL
        },
        // instagram will send back the token and profile
        function(token, refreshToken, profile, done) {

            // asynchronous
            process.nextTick(function () {

                // find the user in the database based on their facebook id
                User.findOne({'kakaotalk.id': profile.id}, function (err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (user) {
                        return done(null, user); // user found, return that user
                    } else {
                        console.log(profile);
                        // if there is no user found with that kakao id, create them
                        var newUser            = new User();

                        // set all of the kakao information in our user model
                        newUser.kakaotalk.id        = profile.id; // set the users kakao id
                        newUser.kakaotalk.name      = profile.username; // look at the connectAccount user profile to see how names are returned
                        newUser.kakaotalk.provider  = 'kakaotalk'; // kakao can return multiple emails so we'll take the first
                        newUser.kakaotalk.json     = profile._json;
                        newUser.isNewUser           = true;
                        var date = new Timestamp();
                        newUser.signupDate          = date.yyyymmdd();
                        // save our user to the database
                        newUser.save(function(err) {
                            if (err)
                                throw err;

                            //if successful, return the new user
                            return done(null, newUser);
                        });
                    }

                });
            });
        }
    ));

};