/**
 * Created by samsi on 2017-01-12.
 */
// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
var Admin = require('../app/models/admin');

// load the auth variables
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        Admin.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('admin-signup', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'userId',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, userId, password, done) {
            console.log(req.body);
            // asynchronous
            // Admin.findOne wont fire unless data is sent back
            process.nextTick(function() {

                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                Admin.findOne({ 'userId' :  userId }, function(err, admin) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);

                    // check to see if theres already a user with that email
                    if (admin) {
                        return done(null, false, req.flash('signupMessage', '같은 ID가 이미 존재합니다.'));
                    } else {

                        // if there is no user with that email
                        // create the user
                        newAdmin = new Admin();
                        // set the user's local credentials
                        newAdmin.userId    = userId;
                        newAdmin.name    = req.body.name;
                        newAdmin.password = newAdmin.generateHash(password);
                        console.log("before Save");
                        // save the user
                        newAdmin.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newAdmin);
                        });
                    }

                });

            });

        })
    );
    passport.use('admin-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'userId',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, userId, password, done) { // callback with email and password from our form
            console.log(req.body);
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            Admin.findOne({ 'userId' :  userId }, function(err, admin) {
                console.log("admin: " + admin);
                // if there are any errors, return the error before anything else
                if (err)
                    return done(err);

                // if no user is found, return the message
                if (!admin){
                    console.log("no such user");
                    return done(null, false, req.flash('loginMessage', 'No admin user found.')); // req.flash is the way to set flashdata using connect-flash
                }

                // if the user is found but the password is wrong
                if (!admin.validPassword(password)){
                    console.log("invalid password");
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
                }
                if (admin.position == 'new'){
                    console.log("user level is new!");
                    return(null, false, req.flash('loginMessage', "권한이 없습니다. 관리자에게 문의하세요."))

                }
                // all is well, return successful user
                return done(null, admin);
            });

        })
    );


};