// config/passport.js

// load all the things we need
import * as FacebookStrategy    from 'passport-facebook'
import * as GoogleStrategy      from 'passport-google-oauth';
import * as NaverStrategy       from 'passport-naver';
import * as InstagramStrategy   from 'passport-instagram';
import * as KakaoStrategy       from 'passport-kakao';

import configAuth               from './auth';
import User                     from './../models/user';

var basic       =             require('./../controllers/basic');
import Timestamp                from './../controllers/getDate';

export default function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
    // =========================================================================
    // NAVER ===================================================================
    // =========================================================================

    passport.use(new NaverStrategy.Strategy({
            clientID: configAuth.naverAuth.clientID,
            clientSecret: configAuth.naverAuth.clientSecret,
            callbackURL: configAuth.naverAuth.callbackURL,
            passReqToCallback: true

        },
        function(req, accessToken, refreshToken, profile, done) {
            if (!req.user) {
                process.nextTick(function () {

                    // find the user in the database based on their facebook id
                    User.findOne({'naver.id': profile.id}, function (err, user) {

                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database
                        if (err)
                            return done(err);

                        // if the user is found, then log them in
                        if (user) {
                            console.log("exisiting user");
                            return done(null, user); // user found, return that user
                        } else {
                            // if there is no user, create them
                            var newUser                 = new User();
                            console.log(profile);
                            // set all of the user data that we need
                            newUser.naver.id            = profile.id;
                            newUser.naver.name          = profile._json.nickname;
                            newUser.naver.displayName   = profile._json.nickname;
                            newUser.naver.email         = profile.emails[0].value;
                            newUser.naver.provider      = 'naver';
                            newUser.naver.json          = profile._json;
                            newUser.isNewUser           = true;

                            newUser.userInfo.nickname   = profile._json.nickname;
                            newUser.userInfo.email      = profile.email;

                            var date = new Timestamp();
                            newUser.signupDate          = date.yyyymmdd();
                            // save our user into the database
                            newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            });
                        }

                    });
                });
            }
            else{
                User.findOne({'naver.id': profile.id}, function (err, userToMerge) {
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (userToMerge) {
                        mergeUser('naver', userToMerge, req, function(user){
                            return done(null, user);
                        });
                    } else {
                        User.findById(req.user._id, function (err, user){
                            if (err) return done(err);
                            console.log("find existing user by user._id");

                            // set all of the relevant information
                            user.naver.id            = profile.id;
                            user.naver.name          = profile._json.name;
                            user.naver.displayName   = profile._json.nickname;
                            user.naver.email         = profile.emails[0].value;
                            user.naver.provider      = 'naver';
                            user.naver.json          = profile._json;
                            user.isNewUser           = true;

                            user.save(function(err){
                                if (err) throw err;
                                return done(null, user);
                            })
                        })
                    }
                });

            }
        }
    ));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy.OAuth2Strategy({

            clientID        : configAuth.googleAuth.clientID,
            clientSecret    : configAuth.googleAuth.clientSecret,
            callbackURL     : configAuth.googleAuth.callbackURL,
            passReqToCallback: true,

        },
        function(req, token, refreshToken, profile, done) {
            if (!req.user) {
                process.nextTick(function () {

                    // find the user in the database based on their facebook id
                    User.findOne({'google.id': profile.id}, function (err, user) {

                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database
                        if (err)
                            return done(err);

                        // if the user is found, then log them in
                        if (user) {
                            return done(null, user); // user found, return that user
                        } else {
                            // if the user isnt in our database, create a new user
                            var newUser          = new User();

                            // set all of the relevant information
                            newUser.google.id    = profile.id;
                            newUser.google.token = token;
                            newUser.google.name  = profile.displayName;
                            newUser.google.email = profile.emails[0].value; // pull the first email
                            newUser.google.json = profile._json;
                            newUser.userInfo.name       = profile.displayName;
                            newUser.google.provider = 'google'
                            newUser.google.photo = profile.photos[0].value; // pull the first email

                            newUser.userInfo.name       = profile.displayName;
                            newUser.userInfo.email      = profile.email;

                            newUser.isNewUser           = true;
                            var date = new Timestamp();
                            newUser.signupDate          = date.yyyymmdd();
                            // save the user
                            newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            });
                        }

                    });
                });
            }
            else{
                User.findOne({'google.id': profile.id}, function (err, userToMerge) {
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (userToMerge) {
                        mergeUser('google', userToMerge, req, function(user){
                            return done(null, user);
                        });
                    } else {
                        User.findById(req.user._id, function (err, user){
                            if (err) return done(err);
                            console.log("find existing user by user._id");

                            // set all of the relevant information
                            user.google.id    = profile.id;
                            user.google.token = token;
                            user.google.name  = profile.displayName;
                            user.google.email = profile.emails[0].value; // pull the first email
                            user.google.photo = profile.photos[0].value; // pull the first email
                            user.google.provider = 'google'
                            user.google.json = profile._json;

                            user.save(function(err){
                                if (err) throw err;
                                return done(null, user);
                            })
                        })
                    }
                });

            }

        }
    ));


    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy.Strategy({

            // pull in our app id and secret from our auth.js file
            clientID        : configAuth.facebookAuth.clientID,
            clientSecret    : configAuth.facebookAuth.clientSecret,
            callbackURL     : configAuth.facebookAuth.callbackURL,
            profileFields   : ["email", "displayName", 'id', 'picture.type(large)'],
            passReqToCallback: true
        },

        // facebook will send back the token and profile
        function(req, token, refreshToken, profile, done) {
            console.log(profile);

            if (!req.user) {
                process.nextTick(function () {

                    // find the user in the database based on their facebook id
                    User.findOne({'facebook.id': profile.id}, function (err, user) {

                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database
                        if (err)
                            return done(err);

                        // if the user is found, then log them in
                        if (user) {
                            return done(null, user); // user found, return that user
                        } else {
                            // if there is no user found with that facebook id, create them
                            var newUser            = new User();
                            // set all of the facebook information in our user model
                            newUser.facebook.id    = profile.id; // set the users facebook id
                            newUser.facebook.token = token; // we will save the token that facebook provides to the user
                            newUser.facebook.name  = profile.displayName; // look at the passport user profile to see how names are returned
                            newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                            newUser.facebook.photo = "https://graph.facebook.com/" + profile.username + "/picture" + "?width=200&height=200" + "&access_token=" + token;
                                newUser.facebook.provider = 'facebook';
                            newUser.facebook.json = profile._json;

                            newUser.userInfo.name       = profile.displayName;
                            newUser.userInfo.email      = profile.email;

                            newUser.isNewUser           = true;
                            var date = new Timestamp();
                            newUser.signupDate          = date.yyyymmdd();


                            newUser.userInfo.name       = profile._json.displayName;
                            // save our user to the database
                            newUser.save(function(err) {
                                if (err)
                                    throw err;

                                // if successful, return the new user
                                return done(null, newUser);
                            });
                        }

                    });
                });
            }
            else{
                User.findOne({'facebook.id': profile.id}, function (err, userToMerge) {
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (userToMerge) {
                        mergeUser('facebook', userToMerge, req, function(user){
                            return done(null, user);
                        });
                    } else {
                console.log("connect facebook to existing account!");
                User.findById(req.user._id, function (err, user){
                    if (err) return done(err);
                    console.log("find existing user by user._id");

                    // set all of the relevant information
                    user.facebook.id    = profile.id; // set the users facebook id
                    user.facebook.token = token; // we will save the token that facebook provides to the user
                    user.facebook.name  = profile.displayName; // look at the passport user profile to see how names are returned
                    user.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                    user.facebook.photo = profile.photos[0].value;
                    user.facebook.provider = 'facebook';
                    user.facebook.json = profile._json;

                    user.save(function(err){
                        if (err) throw err;
                        return done(null, user);
                    })
                })
                }
            });

}

}
));


// =========================================================================
    // INSTAGRAM ===============================================================
    // =========================================================================
    passport.use(new InstagramStrategy.Strategy({
            clientID: configAuth.instagramAuth.clientID,
            clientSecret: configAuth.instagramAuth.clientSecret,
            callbackURL: configAuth.instagramAuth.callbackURL,
            passReqToCallback: true,
        },
        // instagram will send back the token and profile
        function(req, token, refreshToken, profile, done) {
            console.log(profile);
            // asynchronous
            if (!req.user) {
                process.nextTick(function () {

                    // find the user in the database based on their facebook id
                    User.findOne({'instagram.id': profile.id}, function (err, user) {

                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database
                        if (err)
                            return done(err);

                        // if the user is found, then log them in
                        if (user) {
                            return done(null, user); // user found, return that user
                        } else {
                            // if there is no user found with that facebook id, create them
                            var newUser            = new User();

                            // set all of the facebook information in our user model
                            newUser.instagram.id    = profile.id; // set the users facebook id
                            newUser.instagram.name = profile.username; // we will save the token that facebook provides to the user
                            newUser.instagram.displayName  = profile.displayName; // look at the passport user profile to see how names are returned
                            newUser.instagram.json          = profile._json;
                            newUser.instagram.provider      = 'instagram';

                            newUser.userInfo.name       = profile.username;
                            newUser.userInfo.nickname   = profile.displayName;

                            newUser.isNewUser                 = true;
                            var date = new Timestamp();
                            newUser.signupDate          = date.yyyymmdd();

                            // save our user to the database
                            newUser.save(function(err) {
                                if (err)
                                    throw err;

                                // if successful, return the new user
                                return done(null, newUser);
                            });
                        }

                    });
                });
            }
            else{
                User.findOne({'instagram.id': profile.id}, function (err, userToMerge) {
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (userToMerge) {
                        mergeUser('instagram', userToMerge, req, function(user){
                            return done(null, user);
                        });
                    } else {
                        User.findById(req.user._id, function (err, user){
                            if (err) return done(err);
                            console.log("find existing user by user._id");

                            user.instagram.id    = profile.id; // set the users facebook id
                            user.instagram.name = profile.username; // we will save the token that facebook provides to the user
                            user.instagram.displayName  = profile.displayName; // look at the passport user profile to see how names are returned
                            user.instagram.json          = profile._json;
                            user.instagram.provider      = 'instagram';

                            user.save(function(err){
                                if (err) throw err;
                                return done(null, user);
                            })
                        })
                    }
                });

            }

        }
    ));

    // =========================================================================
    // KAKAOTALK ===============================================================
    // =========================================================================
    passport.use(new KakaoStrategy.Strategy({
            clientID: configAuth.kakaotalkAuth.clientID,
            callbackURL: configAuth.kakaotalkAuth.callbackURL,
            passReqToCallback: true,

        },
        // instagram will send back the token and profile
        function(req, token, refreshToken, profile, done) {
// asynchronous
            if (!req.user) {
                process.nextTick(function () {

                    // find the user in the database based on their facebook id
                    User.findOne({'kakaotalk.id': profile.id}, function (err, user) {

                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database
                        if (err)
                            return done(err);

                        // if the user is found, then log them in
                        if (user) {
                            return done(null, user); // user found, return that user
                        } else {
                            // if there is no user found with that kakao id, create them
                            var newUser            = new User();

                            // set all of the kakao information in our user model
                            newUser.kakaotalk.id        = profile.id; // set the users kakao id
                            newUser.kakaotalk.name      = profile.username; // look at the passport user profile to see how names are returned
                            newUser.kakaotalk.provider  = 'kakaotalk'; // kakao can return multiple emails so we'll take the first
                            newUser.kakaotalk.json     = profile._json;
                            newUser.kakaotalk.photo       =profile.thumbnail_image;

                            newUser.userInfo.name       = profile.username;
                            newUser.isNewUser           = true;
                            var date = new Timestamp();
                            newUser.signupDate          = date.yyyymmdd();
                            // save our user to the database
                            newUser.save(function(err) {
                                if (err)
                                    throw err;

                                //if successful, return the new user
                                return done(null, newUser);
                            });
                        }

                    });
                });
            }
            else{
                User.findOne({'kakaotalk.id': profile.id}, function (err, userToMerge) {
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (userToMerge) {
                        mergeUser('kakaotalk', userToMerge, req, function(user){
                            return done(null, user);
                        });

                    } else {
                        User.findById(req.user._id, function (err, user){
                            if (err) return done(err);
                            console.log("find existing user by user._id");

                            user.kakaotalk.id        = profile.id; // set the users kakao id
                            user.kakaotalk.name      = profile.username; // look at the passport user profile to see how names are returned
                            user.kakaotalk.provider  = 'kakaotalk'; // kakao can return multiple emails so we'll take the first
                            user.kakaotalk.json     = profile._json;
                            user.kakaotalk.photo       =profile.thumbnail_image;
                            user.save(function(err){
                                if (err) throw err;
                                return done(null, user);
                            })
                        })
                    }
                });

            }

        }
    ));

};
function mergeUser(whichSNS, userToMerge, req, callback){

            User.findById(req.user._id, function (err, user){
                if (err) throw err;
                if(userToMerge.signupDate > user.signupDate){
                    var itemsProcessed = 0;

                    var listSNS = ['naver', 'kakaotalk', 'facebook', 'google', 'instagram'];
                    listSNS.forEach(function (e){
                        if(!basic.isEmpty(userToMerge[e].id)){
                            console.log(userToMerge[e].id);
                            user[e] = userToMerge[e];

                        }
                        itemsProcessed++;
                        if(itemsProcessed === listSNS.length) {
                            user.save(function(err){
                                if (err) throw err;
                                userToMerge.remove(function (err){
                                    if (err) throw err;
                                    callback(userToMerge);

                                })
                            })
                        }
                    })

                }else{
                    var itemsProcessed = 0;

                    var listSNS = ['naver', 'kakaotalk', 'facebook', 'google', 'instagram'];
                    listSNS.forEach(function (e){
                        if(!basic.isEmpty(user[e].id)){
                            console.log(user[e].id);
                            userToMerge[e] = user[e];
                        }
                        itemsProcessed++;
                        if(itemsProcessed === listSNS.length) {
                            userToMerge.save(function(err){
                                if (err) throw err;
                                user.remove(function (err){
                                    if (err) throw err;
                                    callback(userToMerge);

                                })
                            })
                        }
                    })
                }

            })



}

