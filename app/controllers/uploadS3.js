import path     from 'path';
import multer   from 'multer';
import multerS3 from 'multer-s3';
import crypto   from 'crypto';
import s3       from './../config/aws';

var uploadProfile = multer({
    storage: multerS3({
        s3: s3,
        bucket: 't-tag',
        metadata: function (req, file, cb) {
            cb(null, {fieldName: file.fieldname});
        },
        key: function (req, file, cb) {
            crypto.pseudoRandomBytes(16, function (err, raw) {
                cb(null, 'user/profile/' + raw.toString('hex') + path.extname(file.originalname));

            })
        }
    })
}).single('profileImage');

export default uploadProfile ;