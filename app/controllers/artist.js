/**
 * Created by SlothTech on 2017-02-27.
 */
/**
 * Created by SlothTech on 2017-02-27.
 */
import Artist           from './../models/artist';
var basic =           require('./basic');
import Timestamp        from './getDate';

function CArtist(){

}
CArtist.prototype.set = function(req, thumb, callback){
    if(!basic.isEmpty(req.body.name)){

        Artist.findOne({'name' : req.body.name}, function(err, artist){
            if(err) callback(err);
            if (artist){
                var date = new Timestamp();
                artist.lastModified = date.yyyymmdd();

                if(!basic.isEmpty(req.body.address))
                    artist.address = req.body.address;
                if(!basic.isEmpty(req.body.hashtag))
                    artist.hashtag = JSON.parse(req.body.hashtag);
                if(!basic.isEmpty(req.body.facebook))
                    artist.facebook = req.body.facebook;
                if(!basic.isEmpty(thumb))
                    artist.profileImg = thumb;
                artist.save(function(err){
                    if (err) throw err;
                    return callback(null, artist);
                })
            }
            else {
                var artist = new Artist();
                var date = new Timestamp();
                artist.lastModified = date.yyyymmdd();
                artist.signupDate = date.yyyymmdd();
                
                artist.name = req.body.name;
                if(!basic.isEmpty(req.body.address))
                    artist.address = req.body.address;
                if(!basic.isEmpty(req.body.hashtag))
                    artist.hashtag = JSON.parse(req.body.hashtag);
                if(!basic.isEmpty(req.body.facebook))
                    artist.facebook = req.body.facebook;
                if(!basic.isEmpty(thumb))
                    artist.profileImg = thumb;
                artist.save(function(err){
                    if (err) throw err;
                    return callback(null, artist);
                })
            }
        });
    }
    else{
        callback(false, "no req.body.name exist");
    }
}
CArtist.prototype.del = function(req, callback){

}
CArtist.prototype.showArtistList = function(callback){

}
CArtist.prototype.showArtistListAdmin = function(callback){
    Artist.find(function(err, artists){
        if(err) throw err;
        var html = '<table class="table table-striped">'+
            '<tr>'+
            '<th class="number">번호</th>'+
            '<th class="name">이름</th>'+
            '<th class="profile">프로필</th>'+
            '<th class="add">상품추가</th>'+
            '<th class="delete">지우기</th>'+
            '</tr>';
        for(var i=0; i<artists.length; i++){
            html+='<tr>'+
                    '<td class="number">'+(i+1)+'</td>'+
                    '<td class="name">'+artists[i].name+'</td>'+
                    '<td class="profile"><img src="'+artists[i].profileImg+'"></td>'+
                    '<td class="add"><a href="/admin/product_add?id='+artists[i]._id+'">상품추가</a></td>'+
                    '<td class="delete"><span class="glyphicon glyphicon-remove"</span></td></tr>';
        }
        html+='</table>';

        console.log(html);
        callback(null, html);
    })

}

CArtist.prototype.drawartistPage = function(callback){

}

CArtist.prototype.drawDetailedPage = function(id, callback){

}
module.exports = CArtist;