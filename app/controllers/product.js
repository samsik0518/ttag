/**
 * Created by SlothTech on 2017-02-27.
 */
/**
 * Created by SlothTech on 2017-02-27.
 */
import Product          from './../models/product';
import Artist           from './../models/artist'
import User             from './../models/user';
var basic =             require('./basic');
import Timestamp        from './getDate';

function CProduct(){

}
CProduct.prototype.set = function(req, thumb, callback){
     if(!basic.isEmpty(req.body.name)){

        Product.findOne({'name' : req.body.name}, function(err, product){
            if(err) throw err;
            if (product){
                var date = new Timestamp();
                product.timeStamp = date.yyyymmdd();

                if(!basic.isEmpty(req.body.color))
                    product.color = JSON.parse(req.body.color);
                if(!basic.isEmpty(req.body.size))
                    product.size = JSON.parse(req.body.size);
                if(!basic.isEmpty(req.body.size))
                    product.color = JSON.parse(req.body.color);
                if(!basic.isEmpty(req.body.category))
                    product.category =req.body.category;
                if(!basic.isEmpty(thumb))
                    product.thumb = thumb;
                if(!basic.isEmpty(req.body.price))
                    product.price = req.body.price;
                if(!basic.isEmpty(req.body.keyword))
                    product.keyword = JSON.parse(req.body.keyword);
                if(!basic.isEmpty(req.body.content))
                    product.content = req.body.content;
                if (!basic.isEmpty(req.body.makingTime))
                    product.makeTime = req.body.makingTime;

                if (!basic.isEmpty(req.body.sleeve))
                    product.fashionCode.sleeve = req.body.sleeve;                
                if (!basic.isEmpty(req.body.neck))
                    product.fashionCode.neck = req.body.neck;
                if (!basic.isEmpty(req.body.gender))
                    product.fashionCode.gender = req.body.gender;
                if (!basic.isEmpty(req.body.fit))
                    product.fashionCode.fit = req.body.fit;
                    
                if (!basic.isEmpty(req.body.onepieceBody))
                    product.fashionCode.onepieceBody = req.body.onepieceBody;
                if (!basic.isEmpty(req.body.onepieceLength))
                    product.fashionCode.onepieceLength = req.body.onepieceLength;
                if (!basic.isEmpty(req.body.topBody))
                    product.fashionCode.topBody = req.body.topBody;
                if (!basic.isEmpty(req.body.bottomType))
                    product.fashionCode.bottomType = req.body.bottomType;
                if (!basic.isEmpty(req.body.bottomLength))
                    product.fashionCode.bottomLength = req.body.bottomLength;
                    
                if(!basic.isEmpty(req.body.age))
                    product.fashionCode.age = JSON.parse(req.body.age);
                if(!basic.isEmpty(req.body.season))
                    product.fashionCode.season = JSON.parse(req.body.season);
                
                Artist.findById(req.body.id, function(err, artist){
                    if(err) throw err;
                    product.seller = artist;
                    
                    product.save(function(err){
                        if (err) throw err;
                        return callback(null, product);
                    })
                });
                
            }
            else {
                var new_product = new Product();
                var date = new Timestamp();
                new_product.timeStamp = date.yyyymmdd();

                if (!basic.isEmpty(req.body.color))
                    new_product.color = JSON.parse(req.body.color);
                if (!basic.isEmpty(req.body.size))
                    new_product.size = JSON.parse(req.body.size);
                if (!basic.isEmpty(req.body.size))
                    new_product.color = JSON.parse(req.body.color);
                if (!basic.isEmpty(req.body.keyword))
                    new_product.keyword = JSON.parse(req.body.keyword);

                if (!basic.isEmpty(req.body.category))
                    new_product.category = req.body.category;
                if (!basic.isEmpty(thumb))
                    new_product.thumb = thumb;
                if (!basic.isEmpty(req.body.price))
                    new_product.price = req.body.price;
                if (!basic.isEmpty(req.body.content))
                    new_product.content = req.body.content;
                if (!basic.isEmpty(req.body.name))
                    new_product.name = req.body.name;
                if (!basic.isEmpty(req.body.makingTime))
                    new_product.makeTime = req.body.makingTime;

                if (!basic.isEmpty(req.body.sleeve))
                    new_product.fashionCode.sleeve = req.body.sleeve;                   
                if (!basic.isEmpty(req.body.neck))
                    new_product.fashionCode.neck = req.body.neck;
                if (!basic.isEmpty(req.body.gender))
                    new_product.fashionCode.gender = req.body.gender;
                if (!basic.isEmpty(req.body.fit))
                    new_product.fashionCodefit = req.body.fit;
                if(!basic.isEmpty(req.body.age))
                    new_product.fashionCode.age = JSON.parse(req.body.age);
                if(!basic.isEmpty(req.body.season))
                    new_product.fashionCode.season = JSON.parse(req.body.season);
                
                Artist.findById(req.body.id, function(err, artist){
                    if(err) throw err;
                    new_product.seller = artist;
                    
                    new_product.save(function(err){
                        if (err) throw err;
                        return callback(null, product);
                    })
                });
                
            }
        });
    }
    else{
        callback(false, "no req.body.name exist");
    }
}
CProduct.prototype.del = function(req, callback){

}
CProduct.prototype.showProductList = function(user, callback){
Product.find()
.populate('seller')
.exec(function(err, products) {
    if(err) throw err;
        var html ='';
        
        for(var i=0; i<products.length; i++){
            var follow_status ='follow';
            if (products[i].seller.follow.indexOf(user) > -1){
                follow_status = 'following';

                
            }
            html+='<article id="'+products[i]._id+'"class="'+products[i].seller._id+' grid-item grid-item-1 col-sm-12 col-md-6 col-lg-4 gender-'+products[i].fashionCode.gender;
            html+=' '+products[i].category;
            for(var j=0; j<products[i].fashionCode.age.length; j++){
                html+=' age-'+products[i].fashionCode.age[j]+' ';
            }
            for(var j=0; j<products[i].color.length; j++){
                html+=' color-'+products[i].color[j]+' ';
            }
             for(var j=0; j<products[i].fashionCode.season.length; j++){
                html+=' season-'+products[i].fashionCode.season[j]+' ';
            }
             for(var j=0; j<products[i].keyword.length; j++){
                html+=' hashtag-'+products[i].keyword[j]+' ';
                html+=' hashtag-'+products[i].keyword[j].replace(/#/g, '');
            }
            var artist = new Array();
            artist = products[i].seller.name.split(/[ ,]+/);
            for(var j=0; j<artist.length; j++){
                html+= ' artist-'+artist[j];
            }
            html+=' artist-'+products[i].seller.name.replace(/\s/g,'');
            html+= ' fit-'+products[i].fashionCode.fit + ' length-' + products[i].fashionCode.length + ' neck-' +products[i].fashionCode.neck + ' type-'+products[i].fashionCode.typeClothes;
            var product = new Array();
            product = products[i].name.split(/[ ,]+/);

            for(var j=0; j<product.length; j++){
                html+=' itemName-'+ product[j];
            }
            html+=' itemName-' + products[i].name.replace(/\s/g,'');
            html+='">';
            html+= '<header><div class="artist-profile-wrapper col-lg-3"><div class="artist-profile"><img src="'+products[i].seller.profileImg+'"></div></div><div class="product-profile col-lg-8 col-lg-push-1">';
            html+= '<p class="artist-name">'+products[i].seller.name+'</p><a class="product-name" href="/product?id='+products[i]._id+'">'+products[i].name+'</a><p class="price"><span class="price_value">'+products[i].price+'</span>원</p></div><div class="clearfix"></div></header>';
            html+=' <main><div class="product-image-wrapper"><div class="product-image"><a href="/product?id='+products[i]._id+'"><img src="'+products[i].thumb+'"></a></div></div><div class="clearfix"></div></main>';
            html+= '<footer><div class="follow-wrapper"><div class="heart-wrapper col-sm-4 col-lg-4 col-md-4"><i class="fa fa-heart-o" aria-hidden="true" onclick=like(this)></i><span class="follow-count" onclick=like(this)> '+products[i].like.count+'</span></div>';
            html+='<div class="comment-wrapper col-sm-4 col-lg-4 col-md-4"><i class="fa fa-comment-o" aria-hidden="true" onclick=comment(this);></i><span class="comment-count" onclick="comment(this)"> '+products[i].comment.length+'</span></div>';
            html+='<div class="follow-btn-wrapper col-sm-4 col-lg-4 col-md-4"><span class="follow-button follow" onclick="follow(this)">'+follow_status+'</span></div><div class="clearfix"></div></div></footer></article>';
    
        }
        callback(null, html);
        
    })
}
CProduct.prototype.showArtistListAdmin = function(callback){
  
}

CProduct.prototype.drawproductPage = function(callback){

}

CProduct.prototype.drawDetailedPage = function(id, callback){
    Product.findById(id)
    .populate({
        path: 'comment.user', 
        model: 'User'})
    .exec(function(err, item) {
        if(err) throw(err);

        var html ='';
        html +='<div class="img-wrapper">';
            html +='<img src="'+item.thumb+'" class="detailed-image">';
            html+='<div class="form-control keyword-wrapper">';
                for(var i=0; i<item.keyword.length; i++){
                    html+='<span class="keyword keyword-'+i+'">'+item.keyword[i]+'</span>';
                }
            html+='</div>'
        html+='</div>';
        html +='<div class="info-wrapper">';
        html +='<h1>'+item.name+'</h1>';
        html +='<div class="form-group">';
            html +='<label for="color" class="col-sm-3 control-label">Color: </label>';
            html +='<div class="col-sm-9 ">';
                html +='<select class="col-sm-9 color form-control" id="color" name="color">';
                for(var i=0; i<item.color.length; i++){
                    html+='<option value="'+item.color[i]+'">'+item.color[i]+'</option>';
                }
                html+='</select>';
            html +='</div>';

            html +='<label for="size" class="col-sm-3 control-label">Size: </label>';
            html +='<div class="col-sm-9 ">';
                html +='<select class="size form-control" id="size" name="size"><option value="mySize">My Size</option>';
                for(var i=0; i<item.size.length; i++){
                    html+='<option value="'+item.size[i]+'">'+item.size[i]+'</option>';
                }
                html+='</select>';
            html +='</div>';
        html+='<div class="clearfix space1"></div>';

            html +='<label class="col-sm-3 control-label">Price: </label>';
        html +='<div class="col-sm-9">';

        html +='<div class="col-sm-9 info form-control">'+item.price+'</div>';
        html+='</div>';
        html+='<div class="clearfix"></div>';

            html +='<label class="col-sm-3 control-label">Making Time: </label>';
            html +='<div class="col-sm-9">';
            html +='<div class="info form-control">'+item.makeTime+'</div>';
        html+='</div>';

        html+='</div>';
        html+='<div class="clearfix space2"></div>';

        html +='<div class="form-group">';
            html+='<button id="cart" class="btn btn-theme btn-custom col-sm-5">Cart</button>';
            html+='<button id="order" class="btn btn-theme btn-custom col-sm-5 col-sm-push-1" disabled>Order<br/>(결제모듈 연동 중)</button>';
            html+='<div class="clearfix"></div>';
            

        html+='</div>';
        html+='</div>';
        html+='<div class="clearfix space2"></div>';
        html+='<div id="detailed-wrapper">'
            for(var i=0; i<item.detailedInfo.length; i++){
                html+='<img class="detailed-img" src="'+item.detailedInfo[i]+'">'
            }
        html+='</div>'
        html+='<div class="clearfix space2"></div>';
        
        
        html+='<div id="comment-wrapper">'
        html+='        <h2>댓글</h2>'
        html+='<div class="comment-writed">'
        html+='</div>';

        for(var i=0; i<item.comment.length; i++){
            if(basic.isEmpty(item.comment[i].user.userInfo) || basic.isEmpty(item.comment[i].user.userInfo.nickname)){
                var name = "익명"
            }else{
                var name = item.comment[i].user.userInfo.nickname;
            }
            html+='<div class="comment-writed">'
            html+='<h3 class="commenter">'+name +'</h3>' + '<p class="comment">'+ item.comment[i].text+'</p>';
            html+='</div>';
            html+='<div class="clearfix"></div>';
        }
        html+='        <form class="form-horizontal ">'
        html+='            <textarea name="addComment" class="comment-add" rows="3"></textarea>'
        html+='            <button id="submitComment"class="btn btn-default">댓글 달기</button>'
        html+='        </form>'
        html+='    </div>'
    callback(null, html);
})

}
CProduct.prototype.likesControl = function(req, callback){
    Product.findById(req.body.product)
    .exec(function(err, docs) {
        if(err) throw err;
        
        if(basic.isEmpty(docs.like.user)){
            var count =0;
            if(basic.isEmpty(docs.like))
            Product.findOneAndUpdate({'_id': docs.id}, {$set: { 'like.count': docs.like.count+1 }, $push: {"like.user": req.user.id}}, {upsert: true, new: true} ,function(err, updated){
                if(err) throw err

                callback(null, updated.like.count);
            });
        }
        else if(docs.like.user.indexOf(req.user.id) > -1){
            Product.findOneAndUpdate({'_id': docs.id}, {$set: { 'like.count': docs.like.count-1 }, $pull: { "like.user" : req.user.id }}, {upsert: true, new: true}, function(err, updated){
                if(err) throw err

                callback(null, updated.like.count);
            });

        }else{
            Product.findOneAndUpdate({'_id': docs.id}, {$set: { 'like.count': docs.like.count+1 }, $push: {"like.user": req.user.id}}, {upsert: true, new: true}, function(err, updated){
                if(err) throw err

                callback(null, updated.like.count);
            });
        }
        
    });
}
CProduct.prototype.followControl = function(product, user, callback){
    Product.findById(product)
    .populate('seller')
    .exec(function(err, product) {
        if(err) throw err;
        
        if (product.seller.follow.indexOf(user) > -1){
            Artist.findOneAndUpdate({'_id': product.seller._id},  {$pull: {"follow": user}}, {upsert: true, new: true}, function(err, updated){
                if(err) throw err

                callback(null, product.seller._id);
            });
        
        }else{
            Artist.findOneAndUpdate({'_id': product.seller._id},  {$push: {"follow": user}}, {upsert: true, new: true}, function(err, updated){
                if(err) throw err

                callback(null, product.seller._id);
            });
        }
        
    })

}
CProduct.prototype.addComment = function(product, comment, user, callback){
    Product.findOneAndUpdate({'_id': product}, {$push: {"comment": {"text": comment, "user": user }}}, {upsert: true, new: true}, function(err, updated){
        if(err) throw err;
        User.findById(user, function(err, user){
            if(err) throw err;
            if(basic.isEmpty(user.userInfo) || basic.isEmpty(user.userInfo.nickname)){
                var name = "익명"
            }else{
                var name = user.userInfo.nickname;
            }
            callback(null, name, comment);

        })
        
    })
    
}
module.exports = CProduct;