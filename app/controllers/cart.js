var Cart            = require('./../models/cart');
var Product            = require('./../models/cart');
var User            = require('./../models/user');

var basic           = require('./basic');
var Timestamp            = require('./getDate');

function CCart(){

}
CCart.prototype.initCart = function(userId, callback){
    var newCart = new Cart();
    newCart.user = userId;
    newCart.save(function(err){
        if (err) throw err;
        callback(null, newCart);
    })
}
CCart.prototype.addCart = function(req, userId, callback) {

    if (basic.isEmpty(req.productId)) {
        callback("product ID was not found");
    }
    else if(basic.isEmpty(userId)){
        callback("user ID was not found");
    }
    else{
        var date = new Timestamp();

        Cart.findOneAndUpdate({'user' : userId}, 
        {$push: {
            "cart" : {
                "timeStamp" : date.yyyymmdd(),
                "size" :req.size,
                "color" : req.color,
                "quantity" : req.quantity,
                "product" : req.productId
        }}}, {upsert: true, setDefaultsOnInsert: true}, function(err, updated){
            if(err) throw err
            callback(null, updated);
        })
           
        
    }
};
CCart.prototype.modifyCart = function(req, callback){
    Cart.findById(req._id, function(err, cart){
        if(err) throw err;
        if(!cart) callback("cart item does not already exist");
        else{
            var date = new Timestamp();
            cart.timeStamp = date.yyyymmdd();
            if(!basic.isEmpty(req.size))
                cart.size = req.size;
            if(!basic.isEmpty(req.color))
                cart.color  = req.color;
            cart.save(function(err){
                if (err) throw err;
                return callback(null, cart);
            })
        }
    })
};
CCart.prototype.drawHtml = function(user, callback){
    Cart.findOne({'user' : user})
    .populate({
        path: 'cart.product', 
        model: 'Product'})
    .exec(function(err, items) {
        if(err) throw (err);
        console.log(items);
        var html = '<table class="table">'+
            '<tr>'+
                '<th class="check col-sm-1"><input id="check-all" type="checkbox" name="chk"></th>'+
                '<th class="product-info col-sm-7">상품정보</th>'+
                '<th class="price col-sm-2">주문금액</th>'+
                '<th class="option col-sm-2">주문수량</th>'+
            '</tr>';
        for(var i=0; i<items.cart.length; i++){
            html+= '<tr>'+
                    '<td class="check col-sm-1"><input type="checkbox" name="chk" value="'+items.cart[i].product._id+'"></td>'+
                    '<td class="product-info"><img class="product-img col-sm-5" src="'+items.cart[i].product.thumb+'">'+
                    '<h2 class="product-name">'+items.cart[i].product.name+'</h2>';
            for(var j=0; j<items.cart[i].product.keyword.length; j++){
                html+='<p class="keyword keyword-'+i+'">'+items.cart[i].product.keyword[j]+'</p>';
            }
            html+='</td>';
            html+='<td class="price" data-price="'+items.cart[i].product.price+'">'+items.cart[i].product.price+'원</td>';
            html+='<td class="option">';
            html +='<label for="color" class="col-sm-4 control-label">Size: </label>';
            html +='<div class="col-sm-8 ">';
            html +='<select class="option-size form-control" id="size-'+i+'" name="size"><option value="mySize">My Size</option>';
            for(var j=0; j<items.cart[i].product.size.length; j++){
                html+='<option value="'+items.cart[i].product.size[j]+'">'+items.cart[i].product.size[j]+'</option>';
            }
            html+='</select>';
            html +='</div>';

            html +='<label for="color" class="col-sm-4 control-label">Color: </label>';
            html +='<div class="col-sm-8 ">';
            html +='<select class="option-color form-control" id="color-'+i+'" name="color">';
            for(var j=0; j<items.cart[i].product.color.length; j++){
                html+='<option value="'+items.cart[i].product.color[j]+'">'+items.cart[i].product.color[i]+'</option>';
            }
            html+='</select>';
            html +='</div>';

            html +='<label for="color" class="col-sm-4 control-label">Quantity: </label>';
            html +='<div class="col-sm-8 ">';

            html +='<input type="number" class="form-control" value="'+items.cart[i].quantity+'">'
            html +='</div>';
            html+='</td></tr>'
            html+= '<script>'+
                '$("#size-'+i+'").val("'+items.cart[i].size+'");'+
                '$("#color-'+i+'").val("'+items.cart[i].color+'");'+

                '</script>'
        }
        html+= '</table>';
        
        

        callback(null, html);

    })
}

module.exports = CCart;