function Timestamp (){

}

Timestamp.prototype.yyyymmdd = function() {
    var d = new Date(),
        second = ''+d.getSeconds(),
        minute = ''+ d.getMinutes(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    //return [year, month, day, minute, second].join('-');
    return d;
};

module.exports = Timestamp;