/**
 * Created by samsi on 2017-01-03.
 */
var Onepiece            = require('./../models/onepiece');

function getOnepiece(){

}

getOnepiece.prototype.neckline = function(callback) {

    var query = Onepiece.find();
    query.where('neck').exists('neck.filepath');
    query.exec(function(err, docs){
        if(err) throw err;
        else {

            var neckline ='<div class="wrapper">';

            for(i=0; i<docs.length; i++){
                console.log(i);
                neckline +='<img id="neck'+i+'" class="neckline" data-time="'+docs[i].neck.time+'" data-price="'+docs[i].neck.price+'" data-src="'+docs[i].neck.filepath+'" src="'+docs[i].neck.thumbpath+'" onclick="neck('+i+');" />';
            }
            neckline+='</div>'
            callback(neckline);
        }
    })
};
getOnepiece.prototype.bodyShape = function(callback) {

    var query = Onepiece.find();
    query.where('body').exists('body.filepath');
    query.exec(function(err, docs){
        if(err) throw err;
        else {

            var bodyShape ='<div class="wrapper">';

            for(i=0; i<docs.length; i++){
                console.log(i);
                bodyShape +='<img id="body'+i+'" class="body" data-time="'+docs[i].body.time+'" data-price="'+docs[i].body.price+'" data-src="'+docs[i].body.filepath+'" src="'+docs[i].body.thumbpath+'" onclick="bodyShape('+i+');" />';
            }
            bodyShape +='</div>'
            callback(bodyShape);
        }
    })
};
getOnepiece.prototype.sleevesStyle = function(callback) {

    var query = Onepiece.find();
    query.where('sleeves').exists('sleeves.filepath');
    query.exec(function(err, docs){
        if(err) throw err;
        else {

            var sleevesStyle ='<div class="wrapper">';

            for(i=0; i<docs.length; i++){
                console.log(i);
                sleevesStyle +='<img id="sleeves'+i+'" class="sleeves" data-time="'+docs[i].sleeves.time+'" data-price="'+docs[i].sleeves.price+'" data-src="'+docs[i].sleeves.filepath+'" src="'+docs[i].sleeves.thumbpath+'" onclick="sleeves('+i+');" />';
            }
            sleevesStyle +='</div>';

            callback(sleevesStyle);
        }
    })
};
module.exports = getOnepiece;