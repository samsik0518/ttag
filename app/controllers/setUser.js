/**
 * Created by samsi on 2017-01-07.
 */
/**
 * Created by samsi on 2017-01-03.
 */
var User            = require('./../models/user');
var basic           = require('./basic');

function setUser(){

}
setUser.prototype.setTerms = function(req, callback){
    console.log("setUser Start");
    if(!basic.isEmpty(req.body.terms)){
        console.log("Terms Exist: " + req.body.terms);

        User.findById(req.user._id, function(err, user){
            if(err) throw err;
            if(req.body.terms == 'true' || req.body.terms) user.userInfo.terms = true;
            else user.userInfo.terms = false;
        }).save().exec(callback());
    }
    else{
        console.log("Terms !Exist");

        callback();
    }
}

setUser.prototype.setPrivateInfo = function(req, callback){
    if(!basic.isEmpty(req.body.privateInfo)){
        User.findById(req.user._id, function(err, user){
            if(err) throw err;
            if(req.body.privateInfo) user.userInfo.privateInfo = true;
            else user.userInfo.privateInfo = false;
        }).save().exec(callback());
    }else{
        callback();
    }
}
setUser.prototype.setAdvertise = function (req, callback){
    if(!basic.isEmpty(req.body.advertise)){
        User.findById(req.user._id, function(err, user){
            if(err) throw err;
            if(req.body.advertise) user.userInfo.advertise = true;
            user.userInfo.advertise = false;
        }).save().exec(callback);
    }
    else{
        callback();
    }
}

module.exports = setUser;