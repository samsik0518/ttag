// app/routes.js
//Modules
import express          from 'express';
import passport         from 'passport';

//Models
import Terms            from '../models/term';
import User             from '../models/user';

//Controllers
import uploadProfile    from '../controllers/uploadS3';
import {isEmpty}        from '../controllers/basic';

//Partial Views
import header           from '../controllers/partial/header';
import footer           from '../controllers/partial/footer';
import navSidebar       from '../controllers/partial/navSidebar';
import searchSidebar    from '../controllers/partial/searchSidebar';

import CProduct         from '../controllers/product';
import CCart            from '../controllers/cart';

var router = express.Router();

router
    .get('/',function (req, res) {
        var product = new CProduct();
        var user='';
        if(!isEmpty(req.user)){
            user=req.user._id;
        }
        product.showProductList(user, function(err, article){
            if(err) throw err;
            if ( req.isAuthenticated() ) {

                if(req.user.isNewUser)
                    res.redirect('/setUser'); 
                else
                    // load the index.ejs file
                    res.render('index.ejs',{ title: 'T-Tag', isLoggedIn: true , header: header, footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar, article:article }); // load the index.ejs file
    
                }
            else
                // load the index.ejs file
                res.render('index.ejs',{ title: 'T-Tag', isLoggedIn: false, header: header,footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar, article: article }); // load the index.ejs file

        })
        
    })
    .get('/signin', function(req, res){
	if(req.isAuthenticated()){
		res.redirect('/');
	}else{
		res.render('signin.ejs', {title: 'T-Tag | Sign In', header: header,footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar});

	}
	
	})
	// =====================================
    // LOGOUT ROUTES =======================
    // =====================================
    .get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    })
    .get('/noitem', function(req, res){
        if ( req.isAuthenticated() ) {
            if(req.user.isNewUser)
                res.redirect('/setUser'); // load the index.ejs file
            else
                res.render('noitem.ejs',{ title: 'T-Tag | 해당하는 상품을 찾을 수 없습니다.', isLoggedIn: true }); // load the index.ejs file

        }
        else
            res.render('noitem.ejs', { title: 'T-Tag | 해당하는 상품을 찾을 수 없습니다.', isLoggedIn: false , header: header,footer : footer ,navSidebar:navSidebar, searchSidebar:searchSidebar}); // load the index.ejs file

    })
    .get('/product', function(req, res){
        console.log(req.query.id);
        if(isEmpty(req.query.id)){
            res.redirect('/');
        }
        else {
            var Product = new CProduct();
            Product.drawDetailedPage(req.query.id, function(err, item){
                if(err){
                    if (req.user) {
                        var sign_info = '<button id="logout" onclick="location.href=\'/logout\'">Log Out</button>'
                        res.render('product.ejs', {
                            title: "T-Tag | Detailed - Product ID:"+req.id,
                            isLoggedIn: true,
                            header: header,footer : footer ,navSidebar:navSidebar, searchSidebar:searchSidebar,
                            item: "요청하신 페이지가 없습니다."
                        });

                    } else {
                        res.render('product.ejs', {
                            title: "T-Tag | Detailed - Product ID:"+req.id,
                            isLoggedIn: false,
                            header: header,footer : footer ,navSidebar:navSidebar, searchSidebar:searchSidebar,
                            item: "요청하신 페이지가 없습니다."
                        });
                    }
                }
                else{
                    if (req.user) {
                        var sign_info = '<button id="logout" onclick="location.href=\'/logout\'">Log Out</button>'
                        res.render('product.ejs', {
                            title: "T-Tag | Detailed - Product ID:"+req.id,
                            isLoggedIn: true,
                            header: header,footer : footer ,navSidebar:navSidebar, searchSidebar:searchSidebar,
                            items: item
                        });

                    } else {
                        res.render('product.ejs', {
                            title: "T-Tag | Detailed - Product ID:"+req.id,
                            isLoggedIn: false,
                            header: header,footer : footer ,navSidebar:navSidebar, searchSidebar:searchSidebar,
                            items: item
                        });
                    }
                }
            })
        }
    })
    .post('/like', isLoggedIn, function(req, res){
        console.log("product id: " + req.body.product);
        if(!isEmpty(req.body.product)){
            var product = new CProduct();
            product.likesControl(req, function(err, count){
                if(err) throw err;
                res.json({success: true, count: count})
            });
            
        }
        else{
            res.json({success: false, msg: "no product id matched!"})
        }
    })
    .post('/follow', isLoggedIn, function(req, res){
        if(!isEmpty(req.body.product)){
            var product = new CProduct();
            product.followControl(req.body.product, req.user._id, function(err, msg){
                if(err) throw err;
                res.json({success: true, msg: msg})
            });
            
        }
        else{
            res.json({success: false, msg: "some error happened!"})
        }

    })
    .post('/addComment', isLoggedIn, function(req, res){
        if(!isEmpty(req.body.product)){
            var product = new CProduct();
            product.addComment(req.body.product, req.body.comment, req.user._id, function(err, nickname, comment){
                if(err) throw err;
                res.json({success: true, nickname: nickname, comment: comment})
            });
            
        }
        else{
            res.json({success: false, msg: "some error happened!"})
        }
    })
    .get('/artist', function(req, res){
        res.render('artist.ejs', {title: 'T-Tag | 아티스트로 참여하기', isLoggedIn: true, header: header,footer : footer ,navSidebar:navSidebar, searchSidebar:searchSidebar})
    })
    // =====================================
    // SET INFO ROUTES =====================
    // =====================================
    .get('/setUser', isLoggedIn, function(req, res) {

        var social_account = [];
        var checked = [];
        var name="", nickname="", phone="", postalCode="", basicAddr="", detailAddr="", email="", birthday="", size="", gender="", height="", weight="", color="", style="";
        var p_name, p_nickname, p_phone, p_postalCode, p_basicAddr, p_detailAddr, p_email="", p_birthday="", p_size="", p_gender="", p_height="", p_weight="", p_color="";
        var advDm = false, advEmail=false;
        if(req.user) {
            if (!isEmpty(req.user.naver.provider)) {
                social_account.push("naver");
            }
            if (!isEmpty(req.user.google.provider)) {
                social_account.push("google");
            }
            if (!isEmpty(req.user.kakaotalk.provider)) {
                social_account.push("kakaotalk");
            }
            if (!isEmpty(req.user.facebook.provider)) {
                social_account.push("facebook");
            }
            if (!isEmpty(req.user.instagram.provider)) {
                social_account.push("instagram");

            }
            User.findById(req.user._id, function(err, user){
                console.log(user.userInfo.terms);
                if(!isEmpty(user.userInfo.terms)) {
                    checked.push({"terms" : user.userInfo.terms});
                }
                if(!isEmpty(user.userInfo.privateInfo)) {
                    checked.push({"privacy" : user.userInfo.privateInfo});
                }
                if(!isEmpty(user.userInfo.advertise)) {
                    checked.push({"advertise" : user.userInfo.advertise});
                }
                console.log(checked);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                if(!isEmpty(user.userInfo.name)){
                    name = user.userInfo.name;
                }else{
                    p_name = "이름을 입력해주세요.";
                }
                if(!isEmpty(user.userInfo.nickname)){
                    nickname = user.userInfo.nickname;
                }else{
                    p_nickname="별명을 입력해주세요."
                }
                if(!isEmpty(user.userInfo.phone)){
                    console.log("phone : " +user.userInfo.phone)
                    phone= user.userInfo.phone;
                }else{
                    p_phone="'-' 없이 입력해주세요."
                }
                if(!isEmpty(user.userInfo.postalCode)){
                    postalCode = user.userInfo.postalCode;
                }else{
                    p_postalCode="000-000"
                }
                if(!isEmpty(user.userInfo.basicAddr)){
                    basicAddr = user.userInfo.basicAddr;
                }else{
                    p_basicAddr="주소 검색하기 버튼을 눌러주세요."
                }
                if(!isEmpty(user.userInfo.detailAddr)){
                    detailAddr = user.userInfo.detailAddr;
                }else{
                    p_detailAddr="상세 주소를 입력해주세요."
                }
                if(!isEmpty(user.userInfo.email)){
                    email = user.userInfo.email;
                }else{
                    p_email ="사용하는 이메일 주소를 입력해주세요."
                }
                if(!isEmpty(user.userInfo.birthday)){
                    birthday = user.userInfo.birthday;
                }else{
                    p_birthday ="1900 / 01 / 01 여덟자리 숫자로 입력해주세요"
                }
                if(!isEmpty(user.userInfo.advDm)){
                    advDm = user.userInfo.advDm;
                }else{
                    advDm = false;
                }
                if(!isEmpty(user.userInfo.advEmail)){
                    advEmail = user.userInfo.advEmail;
                }else{
                    advEmail = false;
                }
                if(!isEmpty(user.userInfo.fashionCode.size)){
                    size = user.userInfo.fashionCode.size;
                }else{
                    p_size = "평소 기성복 상의 사이즈";
                }
                if(!isEmpty(user.userInfo.fashionCode.gender)){
                    gender = user.userInfo.fashionCode.gender;
                }else{
                    gender = "female";
                    p_gender = "성별: 남성, 여성";
                }
                if(!isEmpty(user.userInfo.fashionCode.height)){
                    height = user.userInfo.fashionCode.height;
                }else{
                    p_height = "키(cm)";
                }
                if(!isEmpty(user.userInfo.fashionCode.weight)){
                    weight = user.userInfo.fashionCode.weight;
                }else{
                    p_weight = "체중(kg)";
                }
                if(!isEmpty(user.userInfo.fashionCode.color)){
                    color = user.userInfo.fashionCode.color;
                }
                if(!isEmpty(user.userInfo.fashionCode.style)){
                    style = user.userInfo.fashionCode.style;
                }
                var fashion_code="", p_fashion_code="";

               user.getProfileImage(function (img){
                   var profile_image = img;
                   Terms.findOne().sort({ field: 'asc', _id: -1 }).limit(1).exec(function (err, term) {
                       var terms ={}

                       if(!term){
                           terms.terms = "이용약관을 등록하세요";
                           terms.privacy_required = "개인정보수집이용동의(필수)를 등록하세요";
                           terms.privacy_choosable = "개인정보수집이용동의(선택)를 등록하세요";
                           terms.advertise = "광고정보수신동의를 등록하세요";

                       }else{
                           if(isEmpty(term.terms)){
                               terms.terms = "이용약관을 등록하세요";
                           }else{
                               terms.terms = term.terms;
                           }
                           if(isEmpty(term.privacy_required)){
                               terms.privacy_required = "개인정보수집이용동의(필수)를 등록하세요";
                           }else{
                               terms.privacy_required = term.privacy_required;
                           }
                           if(isEmpty(term.privacy_choosable)){
                               terms.privacy_choosable = "개인정보수집이용동의(선택)를 등록하세요";
                           }else{
                               terms.privacy_choosable = term.privacy_choosable;
                           }
                           if(isEmpty(term.advertise)){
                               terms.advertise = "광고정보수신동의를 등록하세요";
                           }else{
                               terms.advertise = term.advertise;
                           }
                       }
                        console.log("color: " + color);
                       var sign_info = '<button id="logout" onclick="location.href=\'/logout\'">Log Out</button>';
                       res.render('setUserInfo.ejs', {
                           title: 'T-Tag | 유저 정보 입력',
                           sign_info: sign_info,
                           terms:              terms.terms,
                           privacy_required:   terms.privacy_required,
                           privacy_choosable:  terms.privacy_choosable,
                           advertise :         terms.advertise,
                           social_account: social_account,
                           checked: checked,
                           name: name,
                           nickname: nickname,
                           phone: phone,
                           postalCode: postalCode,
                           basicAddr : basicAddr,
                           detailAddr: detailAddr,
                           email: email,
                           birthday: birthday,
                           profile_image: profile_image,
                           fashion_code: fashion_code,
                           advDm: advDm,
                           advEmail : advEmail,
                           size: size,
                           gender: gender,
                           height: height,
                           weight: weight,
                           color: color,
                           style: style,
                           p_name: p_name,
                           p_nickname: p_nickname,
                           p_phone: p_phone,
                           p_postalCode: p_postalCode,
                           p_basicAddr : p_basicAddr,
                           p_detailAddr: p_detailAddr,
                           p_email: p_email,
                           p_birthday: p_birthday,
                           p_size: p_size,
                           p_gender: p_gender,
                           p_height: p_height,
                           p_weight: p_weight,
                           header: header,
                           footer:  footer
                           ,navSidebar:navSidebar, searchSidebar:searchSidebar
                           ,isLoggedIn: true ,
                       });
                   });
               });

            })


        }
        else{
            res.redirect('/');
        }
    })
    .get('/setUserRequired', isLoggedIn, function(req, res) {

        var social_account = [];
        var checked = [];
        var name="", nickname="", phone="", postalCode="", basicAddr="", detailAddr="", email="", birthday="", size="", gender="", height="", weight="", color="", style="";
        var p_name, p_nickname, p_phone, p_postalCode, p_basicAddr, p_detailAddr, p_email="", p_birthday="", p_size="", p_gender="", p_height="", p_weight="", p_color="";
        var advDm = false, advEmail=false;
        if(req.user) {
            if (!isEmpty(req.user.naver.provider)) {
                social_account.push("naver");
            }
            if (!isEmpty(req.user.google.provider)) {
                social_account.push("google");
            }
            if (!isEmpty(req.user.kakaotalk.provider)) {
                social_account.push("kakaotalk");
            }
            if (!isEmpty(req.user.facebook.provider)) {
                social_account.push("facebook");
            }
            if (!isEmpty(req.user.instagram.provider)) {
                social_account.push("instagram");

            }
            User.findById(req.user._id, function(err, user){
                console.log(user.userInfo.terms);
                if(!isEmpty(user.userInfo.terms)) {
                    checked.push({"terms" : user.userInfo.terms});
                }
                if(!isEmpty(user.userInfo.privateInfo)) {
                    checked.push({"privacy" : user.userInfo.privateInfo});
                }
                if(!isEmpty(user.userInfo.advertise)) {
                    checked.push({"advertise" : user.userInfo.advertise});
                }
                console.log(checked);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                if(!isEmpty(user.userInfo.name)){
                    name = user.userInfo.name;
                }else{
                    p_name = "이름을 입력해주세요.";
                }
                if(!isEmpty(user.userInfo.nickname)){
                    nickname = user.userInfo.nickname;
                }else{
                    p_nickname="별명을 입력해주세요."
                }
                if(!isEmpty(user.userInfo.phone)){
                    console.log("phone : " +user.userInfo.phone)
                    phone= user.userInfo.phone;
                }else{
                    p_phone="'-' 없이 입력해주세요."
                }
                if(!isEmpty(user.userInfo.postalCode)){
                    postalCode = user.userInfo.postalCode;
                }else{
                    p_postalCode="000-000"
                }
                if(!isEmpty(user.userInfo.basicAddr)){
                    basicAddr = user.userInfo.basicAddr;
                }else{
                    p_basicAddr="주소 검색하기 버튼을 눌러주세요."
                }
                if(!isEmpty(user.userInfo.detailAddr)){
                    detailAddr = user.userInfo.detailAddr;
                }else{
                    p_detailAddr="상세 주소를 입력해주세요."
                }
                if(!isEmpty(user.userInfo.email)){
                    email = user.userInfo.email;
                }else{
                    p_email ="사용하는 이메일 주소를 입력해주세요."
                }
                if(!isEmpty(user.userInfo.birthday)){
                    birthday = user.userInfo.birthday;
                }else{
                    p_birthday ="1900 / 01 / 01 여덟자리 숫자로 입력해주세요"
                }
                if(!isEmpty(user.userInfo.advDm)){
                    advDm = user.userInfo.advDm;
                }else{
                    advDm = false;
                }
                if(!isEmpty(user.userInfo.advEmail)){
                    advEmail = user.userInfo.advEmail;
                }else{
                    advEmail = false;
                }
                if(!isEmpty(user.userInfo.fashionCode.size)){
                    size = user.userInfo.fashionCode.size;
                }else{
                    p_size = "평소 기성복 상의 사이즈";
                }
                if(!isEmpty(user.userInfo.fashionCode.gender)){
                    gender = user.userInfo.fashionCode.gender;
                }else{
                    gender = "female";
                    p_gender = "성별: 남성, 여성";
                }
                if(!isEmpty(user.userInfo.fashionCode.height)){
                    height = user.userInfo.fashionCode.height;
                }else{
                    p_height = "키(cm)";
                }
                if(!isEmpty(user.userInfo.fashionCode.weight)){
                    weight = user.userInfo.fashionCode.weight;
                }else{
                    p_weight = "체중(kg)";
                }
                if(!isEmpty(user.userInfo.fashionCode.color)){
                    color = user.userInfo.fashionCode.color;
                }
                if(!isEmpty(user.userInfo.fashionCode.style)){
                    style = user.userInfo.fashionCode.style;
                }
                var fashion_code="", p_fashion_code="";

               user.getProfileImage(function (img){
                   var profile_image = img;
                   Terms.findOne().sort({ field: 'asc', _id: -1 }).limit(1).exec(function (err, term) {
                       var terms ={}

                       if(!term){
                           terms.terms = "이용약관을 등록하세요";
                           terms.privacy_required = "개인정보수집이용동의(필수)를 등록하세요";
                           terms.privacy_choosable = "개인정보수집이용동의(선택)를 등록하세요";
                           terms.advertise = "광고정보수신동의를 등록하세요";

                       }else{
                           if(isEmpty(term.terms)){
                               terms.terms = "이용약관을 등록하세요";
                           }else{
                               terms.terms = term.terms;
                           }
                           if(isEmpty(term.privacy_required)){
                               terms.privacy_required = "개인정보수집이용동의(필수)를 등록하세요";
                           }else{
                               terms.privacy_required = term.privacy_required;
                           }
                           if(isEmpty(term.privacy_choosable)){
                               terms.privacy_choosable = "개인정보수집이용동의(선택)를 등록하세요";
                           }else{
                               terms.privacy_choosable = term.privacy_choosable;
                           }
                           if(isEmpty(term.advertise)){
                               terms.advertise = "광고정보수신동의를 등록하세요";
                           }else{
                               terms.advertise = term.advertise;
                           }
                       }
                        console.log("color: " + color);
                       var sign_info = '<button id="logout" onclick="location.href=\'/logout\'">Log Out</button>';
                       res.render('setUserRequired.ejs', {
                           title: 'T-Tag | 필수 정보 수정',
                           sign_info: sign_info,
                           terms:              terms.terms,
                           privacy_required:   terms.privacy_required,
                           privacy_choosable:  terms.privacy_choosable,
                           advertise :         terms.advertise,
                           social_account: social_account,
                           checked: checked,
                           name: name,
                           nickname: nickname,
                           phone: phone,
                           postalCode: postalCode,
                           basicAddr : basicAddr,
                           detailAddr: detailAddr,
                           email: email,
                           birthday: birthday,
                           profile_image: profile_image,
                           fashion_code: fashion_code,
                           advDm: advDm,
                           advEmail : advEmail,
                           size: size,
                           gender: gender,
                           height: height,
                           weight: weight,
                           color: color,
                           style: style,
                           p_name: p_name,
                           p_nickname: p_nickname,
                           p_phone: p_phone,
                           p_postalCode: p_postalCode,
                           p_basicAddr : p_basicAddr,
                           p_detailAddr: p_detailAddr,
                           p_email: p_email,
                           p_birthday: p_birthday,
                           p_size: p_size,
                           p_gender: p_gender,
                           p_height: p_height,
                           p_weight: p_weight,
                           header: header,
                           footer: footer,navSidebar:navSidebar, searchSidebar:searchSidebar,
                           isLoggedIn: true ,
                       });
                   });
               });

            })


        }
        else{
            res.redirect('/');
        }
    })
    .get('/setUserAdditional', isLoggedIn, function(req, res) {

        var social_account = [];
        var checked = [];
        var name="", nickname="", phone="", postalCode="", basicAddr="", detailAddr="", email="", birthday="", size="", gender="", height="", weight="", color="", style="";
        var p_name, p_nickname, p_phone, p_postalCode, p_basicAddr, p_detailAddr, p_email="", p_birthday="", p_size="", p_gender="", p_height="", p_weight="", p_color="";
        var advDm = false, advEmail=false;
        if(req.user) {
            if (!isEmpty(req.user.naver.provider)) {
                social_account.push("naver");
            }
            if (!isEmpty(req.user.google.provider)) {
                social_account.push("google");
            }
            if (!isEmpty(req.user.kakaotalk.provider)) {
                social_account.push("kakaotalk");
            }
            if (!isEmpty(req.user.facebook.provider)) {
                social_account.push("facebook");
            }
            if (!isEmpty(req.user.instagram.provider)) {
                social_account.push("instagram");

            }
            User.findById(req.user._id, function(err, user){
                console.log(user.userInfo.terms);
                if(!isEmpty(user.userInfo.terms)) {
                    checked.push({"terms" : user.userInfo.terms});
                }
                if(!isEmpty(user.userInfo.privateInfo)) {
                    checked.push({"privacy" : user.userInfo.privateInfo});
                }
                if(!isEmpty(user.userInfo.advertise)) {
                    checked.push({"advertise" : user.userInfo.advertise});
                }
                console.log(checked);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                if(!isEmpty(user.userInfo.name)){
                    name = user.userInfo.name;
                }else{
                    p_name = "이름을 입력해주세요.";
                }
                if(!isEmpty(user.userInfo.nickname)){
                    nickname = user.userInfo.nickname;
                }else{
                    p_nickname="별명을 입력해주세요."
                }
                if(!isEmpty(user.userInfo.phone)){
                    console.log("phone : " +user.userInfo.phone)
                    phone= user.userInfo.phone;
                }else{
                    p_phone="'-' 없이 입력해주세요."
                }
                if(!isEmpty(user.userInfo.postalCode)){
                    postalCode = user.userInfo.postalCode;
                }else{
                    p_postalCode="000-000"
                }
                if(!isEmpty(user.userInfo.basicAddr)){
                    basicAddr = user.userInfo.basicAddr;
                }else{
                    p_basicAddr="주소 검색하기 버튼을 눌러주세요."
                }
                if(!isEmpty(user.userInfo.detailAddr)){
                    detailAddr = user.userInfo.detailAddr;
                }else{
                    p_detailAddr="상세 주소를 입력해주세요."
                }
                if(!isEmpty(user.userInfo.email)){
                    email = user.userInfo.email;
                }else{
                    p_email ="사용하는 이메일 주소를 입력해주세요."
                }
                if(!isEmpty(user.userInfo.birthday)){
                    birthday = user.userInfo.birthday;
                }else{
                    p_birthday ="1900 / 01 / 01 여덟자리 숫자로 입력해주세요"
                }
                if(!isEmpty(user.userInfo.advDm)){
                    advDm = user.userInfo.advDm;
                }else{
                    advDm = false;
                }
                if(!isEmpty(user.userInfo.advEmail)){
                    advEmail = user.userInfo.advEmail;
                }else{
                    advEmail = false;
                }
                if(!isEmpty(user.userInfo.fashionCode.size)){
                    size = user.userInfo.fashionCode.size;
                }else{
                    p_size = "평소 기성복 상의 사이즈";
                }
                if(!isEmpty(user.userInfo.fashionCode.gender)){
                    gender = user.userInfo.fashionCode.gender;
                }else{
                    gender = "female";
                    p_gender = "성별: 남성, 여성";
                }
                if(!isEmpty(user.userInfo.fashionCode.height)){
                    height = user.userInfo.fashionCode.height;
                }else{
                    p_height = "키(cm)";
                }
                if(!isEmpty(user.userInfo.fashionCode.weight)){
                    weight = user.userInfo.fashionCode.weight;
                }else{
                    p_weight = "체중(kg)";
                }
                if(!isEmpty(user.userInfo.fashionCode.color)){
                    color = user.userInfo.fashionCode.color;
                }
                if(!isEmpty(user.userInfo.fashionCode.style)){
                    style = user.userInfo.fashionCode.style;
                }
                var fashion_code="", p_fashion_code="";

               user.getProfileImage(function (img){
                   var profile_image = img;
                   Terms.findOne().sort({ field: 'asc', _id: -1 }).limit(1).exec(function (err, term) {
                       var terms ={}

                       if(!term){
                           terms.terms = "이용약관을 등록하세요";
                           terms.privacy_required = "개인정보수집이용동의(필수)를 등록하세요";
                           terms.privacy_choosable = "개인정보수집이용동의(선택)를 등록하세요";
                           terms.advertise = "광고정보수신동의를 등록하세요";

                       }else{
                           if(isEmpty(term.terms)){
                               terms.terms = "이용약관을 등록하세요";
                           }else{
                               terms.terms = term.terms;
                           }
                           if(isEmpty(term.privacy_required)){
                               terms.privacy_required = "개인정보수집이용동의(필수)를 등록하세요";
                           }else{
                               terms.privacy_required = term.privacy_required;
                           }
                           if(isEmpty(term.privacy_choosable)){
                               terms.privacy_choosable = "개인정보수집이용동의(선택)를 등록하세요";
                           }else{
                               terms.privacy_choosable = term.privacy_choosable;
                           }
                           if(isEmpty(term.advertise)){
                               terms.advertise = "광고정보수신동의를 등록하세요";
                           }else{
                               terms.advertise = term.advertise;
                           }
                       }
                        console.log("color: " + color);
                       var sign_info = '<button id="logout" onclick="location.href=\'/logout\'">Log Out</button>';
                       res.render('setUserAdditional.ejs', {
                           title: 'T-Tag | 선택 정보 수정',
                           sign_info: sign_info,
                           terms:              terms.terms,
                           privacy_required:   terms.privacy_required,
                           privacy_choosable:  terms.privacy_choosable,
                           advertise :         terms.advertise,
                           social_account: social_account,
                           checked: checked,
                           name: name,
                           nickname: nickname,
                           phone: phone,
                           postalCode: postalCode,
                           basicAddr : basicAddr,
                           detailAddr: detailAddr,
                           email: email,
                           birthday: birthday,
                           profile_image: profile_image,
                           fashion_code: fashion_code,
                           advDm: advDm,
                           advEmail : advEmail,
                           size: size,
                           gender: gender,
                           height: height,
                           weight: weight,
                           color: color,
                           style: style,
                           p_name: p_name,
                           p_nickname: p_nickname,
                           p_phone: p_phone,
                           p_postalCode: p_postalCode,
                           p_basicAddr : p_basicAddr,
                           p_detailAddr: p_detailAddr,
                           p_email: p_email,
                           p_birthday: p_birthday,
                           p_size: p_size,
                           p_gender: p_gender,
                           p_height: p_height,
                           p_weight: p_weight,
                           header: header,
                           footer: footer,navSidebar:navSidebar, searchSidebar:searchSidebar,
                           isLoggedIn: true ,
                       });
                   });
               });

            })


        }
        else{
            res.redirect('/');
        }
    })
    .post('/setUser', function(req, res){
        var cart = new CCart();
        cart.initCart(req.user._id, function(err, callback){
            if(err) throw err;
            User.findById(req.user._id, function(err, user){
                if(err) throw err
                user.setUserInfo(req.body, function (err, userResult){
                    if(err) throw err
    
                    userResult.save(function (err){
                       if(err) throw err
                       res.redirect('/setUser');
                    })
                })
            })
        })
       
    })
    .post('/setUser/profileImage', isLoggedIn, uploadProfile, function(req, res){
            console.log(req.file);
        User.findById(req.user._id, function (err, user){
            if(err) throw err;
            user.userInfo.image = req.file.location;
            user.save(function(err){
                if(err) throw err;
                res.json({'profile_src': user.userInfo.image});
            })
        })

    })

    .post('/setUser/getProfileImage', isLoggedIn, function(req, res){
        User.findById(req.user._id, function (err, user){
            if(err) throw err;
                res.json({'profile_src': user.userInfo.image});
            })

    })


// =====================================
// SET INFO STOPED =====================
// =====================================

    .get('/stopSetUserInfo', function(req, res) {
        if(req.user){
                var user = req.user;
                user.isNewUser = false;
                user.save(function(err){
                    if (err) throw err
                    else{
                        res.redirect('/');
                    }
                })
        }
        else {
            res.redirect('/');
        }
    })
.get('/auth/facebook', passport.authenticate('facebook', { scope : ['email']}))

// handle the callback after facebook has authenticated the user
.get('/auth/facebook/callback',passport.authenticate('facebook', { failureRedirect: '/login' }),  function(req, res) {

    if(req.user.isNewUser){
        console.log("connect new Account!");
        res.redirect('/setUser');
    }else{
        res.redirect('/');
    }
})
// =====================================
// GOOGLE ROUTES =======================
// =====================================
// send to google to do the authentication
// profile gets us their basic information including their name
// email gets their emails
.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }))

// the callback after google has authenticated the user
.get('/auth/google/callback',passport.authenticate('google', { failureRedirect: '/login' }),  function(req, res) {

    if(!isEmpty(req.user.kakaotalk.provider) || !isEmpty(req.user.instagram.provider) || !isEmpty(req.user.facebook.provider) || !isEmpty(req.user.naver.provider) ){
        console.log("connect new Account!");

        res.redirect('/setUser');
    }else{
        res.redirect('/');
    }
})


// =====================================
// NAVER ROUTES ========================
// =====================================
// route for twitter authentication and login
.get('/auth/naver', passport.authenticate('naver'))

// handle the callback after twitter has authenticated the user
.get('/auth/naver/callback',passport.authenticate('naver', { failureRedirect: '/login' }),  function(req, res) {
    console.log("isNewUser? " + req.user.isNewUser);
    if(req.user.isNewUser ){
        console.log("connect new Account!");
        res.redirect('/setUser');
    }else{
        res.redirect('/');
    }
})


// =====================================
// INSTRAGRAM ROUTES ===================
// =====================================
.get('/auth/instagram', passport.authenticate('instagram'))

// handle the callback after instagram has authenticated the user
.get('/auth/instagram/callback',passport.authenticate('instagram', { failureRedirect: '/login' }),
    function(req, res) {
        if(req.user.isNewUser){
            console.log("connect new Account!");

            res.redirect('/setUser');
        }else{
            res.redirect('/');
        }
    })

// =====================================
// KAKAOTALK ROUTES ====================
// =====================================
.get('/auth/kakaotalk', passport.authenticate('kakao'))

// handle the callback after kakaotalk has authenticated the user
.get('/auth/kakaotalk/callback',passport.authenticate('kakao', { failureRedirect: '/login' }),  function(req, res) {

    if(req.user.isNewUser){
        console.log("connect new Account!");

        res.redirect('/setUser');
    }else{
        res.redirect('/');
    }
})
    .get('/privacy', function(req, res){
        
            res.render('privacy.ejs', {title: "T-Tag | Privacy", header: header,footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar,});

    })
    .get('/terms', function(req, res){
        
            res.render('terms.ejs', {title: "T-Tag | Privacy", header: header,footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar,});

    })
    .get('/contact', function(req, res){
        
            res.render('contact.ejs', {title: "T-Tag | Privacy", header: header,footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar,});

    })
    .get('/membership', isLoggedIn, function(req,res){
        if(isEmpty(req.user.image)){
            var profile = "https://s3.ap-northeast-2.amazonaws.com/t-tag/user/profile/silhouette.jpeg"
        }else{
            var profile = req.user.image;
        }
        res.render('membership.ejs',  { title: "T-Tag | Membership", header: header,footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar,
                                        profile: profile, membership: req.user.membership, point: req.user.point})
    })
    .get('/cart', isLoggedIn, function (req, res){

        var Cart = new CCart();
        Cart.drawHtml(req.user._id, function(err, item){
                if(err){
                        res.render('cart.ejs', {
                            title: "T-Tag | Cart",
                            item: "오류가 발생했습니다. 반복되면 관리자에게 문의하십시오.",
                            header: header,footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar,
                            isLoggedIn: true
                        });

                }
                else{
                        res.render('cart.ejs', {
                            title: "T-Tag | Cart",
                            items: item,
                            header: header,footer: footer, navSidebar:navSidebar, searchSidebar:searchSidebar,
                            isLoggedIn: true
                        });

                }
            })
    })
    .post('/addCart', function (req, res){
        console.log(req.body);

            var Cart = new CCart();
            Cart.addCart(req.body, req.user._id, function(err, item){
                if(err){
                    res.send(err);
                }
                else{
                    res.send(200);
                }
            })
    });
export default router;

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}