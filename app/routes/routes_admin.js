// app/routes.js
import express      from 'express';
import Artist       from './../models/artist';
import CArtist      from './../controllers/artist';
import CProduct     from './../controllers/product';

var router = express.Router();

var multer = require('multer');
var aws = require('aws-sdk');
var multerS3 = require('multer-s3');
var path = require('path');
var crypto =require('crypto');

var s3 = new aws.S3({
    region :'ap-northeast-2', //Seoul
    accessKeyId: "AKIAJ2RS4I2ZJNLR4ZVA",
    secretAccessKey: "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"
});

var uploadArtistProfile = multer({
    storage: multerS3({
        s3: s3,
        bucket: 't-tag',
        metadata: function (req, file, cb) {
            cb(null, {fieldName: file.fieldname});
        },
        key: function (req, file, cb) {
            crypto.pseudoRandomBytes(16, function (err, raw) {
                if (err) throw err;
                cb(null, 'artist/profileImg/'+raw.toString('hex')+path.extname(file.originalname));

            })
        }
    })
});
var uploadProductImg = multer({
    storage: multerS3({
        s3: s3,
        bucket: 't-tag',
        metadata: function (req, file, cb) {
            cb(null, {fieldName: file.fieldname});
        },
        key: function (req, file, cb) {
            crypto.pseudoRandomBytes(16, function (err, raw) {
                if (err) throw err;
                cb(null, 'artist/product/'+raw.toString('hex')+path.extname(file.originalname));

            })
        }
    })
});
router
    .get('/',function (req, res) {

        res.render('admin/index',{ title: 'T-Tag', isLoggedIn: false }); // load the index.ejs file

    })
    .get('/artist_add', function(req, res){
        let body ='';

        Artist.schema.eachPath(function(path){
            //console.log(path);
            body+=  '<label for="'+path+'">'+path+'</label>'+'<input type="text"  class="form-control" id="'+path+'">'
        })

        res.render('admin/artist_add', {title: 'T-Tag 수동 아티스트 등록', body_form: body});
    })
    .post('/artist_add', uploadArtistProfile.single('thumb'), function(req, res){
            console.log(req.body);
            console.log(req.file);
            var artist = new CArtist();
            artist.set(req, req.file.location, function(err){
                if(err) throw err;
                res.sendStatus(200);
            })
    })
    .get('/artist_list', function(req, res){
        var artist = new CArtist();
        artist.showArtistListAdmin(function(err, list){
            if(err) throw err;
            res.render('admin/artist_list', {title: 'T-Tag 아티스트 리스트', artist_list: list})
        })
    })
    .get('/product_add', function(req,res){
        res.render('admin/product_add', {title: 'T-Tag 아티스트 상품 추가'});
    })
     .post('/product_add', uploadProductImg.single('thumb'), function(req, res){
            console.log(req.body);
            console.log(req.file);
            var product = new CProduct();
            product.set(req, req.file.location, function(err){
                if(err) throw err;
                res.sendStatus(200);

            })
    
    })

export default router;