import express              from 'express';
import path                 from 'path';
import favicon              from 'serve-favicon';
import logger               from 'morgan';
import cookieParser         from 'cookie-parser';
import bodyParser           from 'body-parser';
import mongoose             from 'mongoose';
import passport             from 'passport'
import passportStg          from './config/passport'; // pass passport for configuration
import flash                from 'connect-flash';
import configDB             from './config/database.js';
import session              from 'express-session';
import routes_shop          from './routes/routes_shop';
import routes_admin          from './routes/routes_admin';


var port = process.env.PORT || '9000';
const app = express();

// configuration ===============================================================
mongoose.Promise = global.Promise;
console.log(configDB.url);
mongoose.connect(configDB.url); // connect to our database
passportStg(passport);

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});
// view engine setup
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'ejs');
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static(path.join(__dirname, '/../public')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());
console.log(path.join(__dirname, '/../public'));
// required for passport
app.use(session({ secret: 'studiojoosworkthebestcompany'})); // session secret
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); // use connect-flash for flash messages stored in session


// routes ======================================================================
app.use('/', routes_shop);
app.use('/admin', routes_admin);


app.set('port', port);
app.listen(port, () => {
    console.log('Express listening on port', port);
});
