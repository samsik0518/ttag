var mongoose = require('mongoose');
var basic           = require('./../controllers/basic');

var termSchema = mongoose.Schema({
    terms               : String,
    privacy_required    : String,
    privacy_choosable   : String,
    advertise           : String
});
termSchema.methods.setTerms = function(req, callback){

        if(!basic.isEmpty(req.terms)){
            this.terms = req.terms;
        }if(!basic.isEmpty(req.privacy_required)){
            this.privacy_required =req.privacy_required;
        }if(!basic.isEmpty(req.privacy_required)){
            this.privacy_choosable =req.privacy_choosable;
        }if(!basic.isEmpty(req.advertise)){
            this.advertise =req.advertise;
        }
        this.save(function(err){
            callback(err);
        })


}

// create the model for terms and expose it to our app
module.exports = mongoose.model('Terms', termSchema);

