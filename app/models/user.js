// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
    isNewUser              : {type: Boolean, default: true},
    signupDate              : Date,
    membership        : {type: String, default: '일반'},
    point       : {type: Number, default: '1000'},
    position : {type: String, default: 'customer'},

    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String,
        provider     : String,
        photo       : String,

        json            : Object
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String,
        provider     : String,

    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String,
        provider     : String,
        photo       : String,

        json            : Object

    },
    naver            : {
        id          : String,
        displayName : String,
        name        : String,
        email       : String,
        provider    : String,
        photo       : String,

        json        : Object
    },
    kakaotalk       : {
        id          : String,
        name:String,
        username: String,
        provider:String,
        roles : String,
        photo       : String,

        json:Object
    },
    instagram       : {
        id: String,
        name: String,
        displayName : String,
        json        : Object,
        provider     : String,
        photo       : String,



    },
    userInfo        : {
        level       : {type: String, defalut: '브론즈'},
        terms       : Boolean,
        privateInfo : Boolean,
        advertise   : Boolean,
        name        : String,
        nickname    : String,
        postalCode  : String,
        basicAddr   : String,
        detailAddr  : String,
        phone       : String,
        email       : String,
        image       : String,
        advDm       : Boolean,
        advEmail    : Boolean,
        birthday    : Number,
        fashionCode : {
            codeName    : String,
            updateDate  : Date,
            size        : String,
            gender      : String,
            height      : Number,
            weight      : Number,
            color       : String,
            style       : [String],
            Measure     : Boolean
        },
        size        : {
            top         : {
                shoulder    : Number,
                chest       : Number,
                stomach     : Number,
                belly       : Number,
                head        : Number,
                neck        : Number,
                forearm     : Number,
                wrist       : Number,
                elbow       : Number,
                hand        : Number
            },
            bottom      : {
                belly       : Number,
                hip         : Number,
                thigh       : Number,
                knee        : Number,
                calf        : Number,
                ankle       : Number,
                heel        : Number,
                centerBack  : Number,
                crotch      : Number,
                kneePosition: Number
            }
        }
    }


});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

userSchema.methods.setUserInfo = function(req, callback){
    console.log("setUser Start");
    if(!isEmpty(req.terms)){

        if(req.terms == 'true' || req.terms) this.userInfo.terms = true;
        else this.userInfo.terms = false;

    }
    if(!isEmpty(req.privacy)){

        if(req.privacy == 'true' || req.privacy == true) this.userInfo.privateInfo = true;
        else this.userInfo.privateInfo = false;

    }
    if(!isEmpty(req.advertise)){

        if(req.advertise == 'true' || req.advertise == true) this.userInfo.advertise = true;
        else this.userInfo.advertise = false;

    }

    if(!isEmpty(req.name)){
        this.userInfo.name = req.name;
    }
    if(!isEmpty(req.nickname)){
        this.userInfo.nickname = req.nickname;
    }

    if(!isEmpty(req.postalCode)){
        this.userInfo.postalCode = req.postalCode;
    }

    if(!isEmpty(req.basicAddr)){
        this.userInfo.basicAddr = req.basicAddr;
    }

    if(!isEmpty(req.detailAddr)){
        this.userInfo.detailAddr = req.detailAddr;
    }

    if(!isEmpty(req.phone)){
        this.userInfo.phone = req.phone;
    }
    if(!isEmpty(req.email)){
        this.userInfo.email = req.email;
    }
    if(!isEmpty(req.advDm)){
        if(req.advDm==true || req.advDm =="true"){
            this.userInfo.advDm = true;
        }else{
            this.userInfo.advDm = false;
        }
    }
    if(!isEmpty(req.advEmail)){
        if(req.advEmail==true || req.advEmail =="true"){
            this.userInfo.advEmail = true;
        }else{
            this.userInfo.advEmail = false;
        }
    }
    if(!isEmpty(req.birthday)){
        this.userInfo.birthday = req.birthday;
    }
    if(!isEmpty(req.size)){
        this.userInfo.fashionCode.size = req.size;
    }
    if(!isEmpty(req.gender)){
        this.userInfo.fashionCode.gender = req.gender;
    }
    if(!isEmpty(req.height)){
        this.userInfo.fashionCode.height = parseFloat(req.height);
    }
    if(!isEmpty(req.weight)){
        this.userInfo.fashionCode.weight = parseFloat(req.weight);
    }
    if(!isEmpty(req.color)){
        var color = JSON.parse(req.color);

                this.userInfo.fashionCode.color = color;
    }
    console.log(req.style);
    console.log(isEmpty(req.style));
    if(!isEmpty(req.style)){
        var style = JSON.parse(req.style);
                this.userInfo.fashionCode.style=style;
    }
    if(!isEmpty(req.measure)){
        if(req.measure==true || req.measure =="true"){
            this.userInfo.fashionCode.measure = true;
        }else{
            this.userInfo.fashionCode.measure = false;
        }
    }
    if(!isEmpty(req.updateDate)){
        this.userInfo.fashionCode.updateDate = req.updateDate;

    }
    callback(null, this);

};
userSchema.methods.getProfileImage =function(callback){
    if(!isEmpty(this.userInfo.image)){
        return callback(this.userInfo.image);
    }
    if(!isEmpty(this.naver.json)){
        if(this.naver.json.profile_image != "https://ssl.pstatic.net/static/pwe/address/nodata_33x33.gif"){
            return callback(this.naver.json.profile_image);
        }
    }

    if(!isEmpty(this.facebook.json)){
        if(!this.facebook.json.picture.data.is_silhouette){
            return callback( this.facebook.json.picture.data.url);
        }
    }
    if(!isEmpty(this.google.json)){
        if(!this.google.json.image.is_default){
            return callback( this.google.json.image.url);
        }
    }
    if(!isEmpty(this.google.json)){
        if(!this.google.json.image.is_default){
            return callback( this.google.json.image.url);
        }
    }
    if(!isEmpty(this.kakaotalk.json)){
        return callback(this.kakaotalk.json.properties.profile_image);
    }
    if(!isEmpty(this.instagram.json)){
        return callback(this.instagram.json.data.profile_picture)
    }
    else{
        return callback("https://ssl.pstatic.net/static/pwe/address/nodata_33x33.gif")
    }
}

function isEmpty(test){
    if(test == 'undefined') return true;
    if(typeof test == 'undefined') return true;
    if(test === 'undefined') return true;
    if(typeof test === 'undefined') return true;

    return false;
}
// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
