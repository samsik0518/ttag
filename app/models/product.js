/**
 * Created by SlothTech on 2017-02-27.
 */
/**
 * Created by samsi on 2017-01-03.
 */
import mongoose   from 'mongoose';
import Artist     from './artist';

var productSchema = mongoose.Schema({
    timeStamp : Date,
    category :  String,
    thumb:      String,
    price:      Number,
    keyword:    [String],
    sample :    [String],
    detailedInfo : [String],
    size :      [String],
    color:      [String],
    name:       String,
    makeTime   :    String,
    published   : {type: Boolean, default: false},
    fashionCode : {
        gender: String,
        age : [Number],
        season : [String],
        fit : String,
        neck : String,
        sleeve: String,
        length: String,
        typeClothes: String,
        onepieceBody: String,
        onepieceLength: String,
        topBody: String,
        bottomType: String,
        bottomLength: String,
    },
    seller :  {type: mongoose.Schema.Types.ObjectId, ref: 'ttagArtist'},
    like : {    count: {type: Number, default: 0},
                user: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
    },
    comment : [ {   text: String,
                user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
    }],

});
// create the model for terms and expose it to our app
module.exports = mongoose.model('Product', productSchema);