/**
 * Created by samsi on 2017-01-03.
 */
var mongoose = require('mongoose');
import User     from './user';
import Product  from './product'

var cartSchema = mongoose.Schema({
    user :  {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    cart : [{
        timeStamp : Date,
        size : String,
        color   : String,
        quantity: {type: Number, default: 1},
        product   : {type: mongoose.Schema.Types.ObjectId, ref: 'Product'}
    }]
    
});

cartSchema.methods.drawHTML = function drawHTML(user, callback) {

    
}

// create the model for terms and expose it to our app
module.exports = mongoose.model('Cart', cartSchema);

