// app/models/ttagUser.js
// load the things we need
var mongoose =  require('mongoose');
import bcrypt   from 'bcrypt-nodejs';
import User     from './user';

// define the schema for our ttagUser model
var ttagArtistSchema = mongoose.Schema({
    signupDate      : Date,
    lastModified    : Date,
    name            : String,
    follow : [ {type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    addr            : String,
    hashtag         : [String],
    facebook        : String,
    instagram       : String,
    profileImg      : String,
    user            : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},



});

// methods ======================
// create the model for ttagUsers and expose it to our app
module.exports = mongoose.model('ttagArtist', ttagArtistSchema);