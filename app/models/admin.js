/**
 * Created by samsi on 2017-01-11.
 */
// app/models/admin.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our admin model
var adminSchema = mongoose.Schema({

    userId          : {type: String, unique: true, require: true},
    password    : {type: String, unique: false, require: true},
    name        : {type: String, unique: false, require: true},
    position    : {type: String, default: 'new'},
    reg_time: {type: Date, default: Date.now},

});

// methods ======================
// generating a hash
adminSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
adminSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

function isEmpty(test){
    if(test == 'undefined') return true;
    if(typeof test == 'undefined') return true;
    if(test === 'undefined') return true;
    if(typeof test === 'undefined') return true;

    return false;
}
// create the model for admins and expose it to our app
module.exports = mongoose.model('Admin', adminSchema);